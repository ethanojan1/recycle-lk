{include file="common/header.tpl"}
{include file="common/top_bar.tpl"}
{include file="common/nav_bar.tpl"}

<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="#">Home</a>
                </li>
                <li>My account</li>
            </ul>

        </div>

        <div class="col-md-12">

            {include file="common/report.tpl"}
        
        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU *** -->
            {include file="common/my_account_sidebar.tpl"}
            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-9">
            <div class="box">
                <h1>My account</h1>
                <p class="lead">Change your personal details or your password here.</p>

                <h3>Change password</h3>

                <form action="my_account.php?job=password" method="POST">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password_old">Old password</label>
                                <input type="password" class="form-control" id="password_old" name="password_old">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password_1">New password</label>
                                <input type="password" class="form-control" id="password_1" name="password_1">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password_2">Retype new password</label>
                                <input type="password" class="form-control" id="password_2" name="password_2">
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->

                    <div class="col-sm-12 text-center">
                        <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save new password</button>
                    </div>
                </form>

                <hr>

                <h3>Personal details</h3>
                <form  action="my_account.php?job=edit_profile" method="POST">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="firstname">Firstname</label>
                                <input type="text" class="form-control" id="firstname" name="first_name" value="{$user_info.first_name}" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="lastname">Lastname</label>
                                <input type="text" class="form-control" id="lastname" name="last_name" value="{$user_info.last_name}" required>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" class="form-control" id="address" name="address" value="{$user_info.address}" required>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-3">
                            <div class="form-group">
                                <label for="zip">ZIP</label>
                                <input type="text" class="form-control" id="zip" name="zip" value="{$user_info.zip}" required>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="form-group">
                                <label for="district">District</label>
                                <select class="form-control" id="district" name="district" required>
                                    {if $user_info.district}
                                        <option value="{$user_info.district}">{$user_info.district}</option>
                                    {else}

                                        <option selected disabled="">Select District</option>
                                    {/if}
                                    <option value="Ampara">Ampara</option>
                                    <option value="Anuradhapura">Anuradhapura</option>
                                    <option value="Badulla">Badulla</option>
                                    <option value="Batticaloa">Batticaloa</option>
                                    <option value="Colombo">Colombo</option>
                                    <option value="Galle">Galle</option>
                                    <option value="Gampaha">Gampaha</option>
                                    <option value="Hambantota">Hambantota</option>
                                    <option value="Jaffna">Jaffna</option>
                                    <option value="Kalutara">Kalutara</option>
                                    <option value="Kandy">Kandy</option>
                                    <option value="Kegalle">Kegalle</option>
                                    <option value="Kilinochchi">Kilinochchi</option>
                                    <option value="Kurunegala">Kurunegala</option>
                                    <option value="Mannar">Mannar</option>
                                    <option value="Matale">Matale</option>
                                    <option value="Matara">Matara</option>
                                    <option value="Moneragala">Moneragala</option>
                                    <option value="Mullaitivu">Mullaitivu</option>
                                    <option value="Nuwara Eliya">Nuwara Eliya</option>
                                    <option value="Polonnaruwa">Polonnaruwa</option>
                                    <option value="Puttalam">Puttalam</option>
                                    <option value="Ratnapura">Ratnapura</option>
                                    <option value="Trincomalee">Trincomalee</option>
                                    <option value="Vavuniya">Vavuniya</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="phone">Telephone</label>
                                <input type="text" class="form-control" id="phone" name="phone" value="{$user_info.phone}" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" id="email" name="email" value="{$user_info.email}" required>
                            </div>
                        </div>
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save changes</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->


{include file="common/footer.tpl"}
{include file="common/copyright.tpl"}
{include file="common/footer_js.tpl"}