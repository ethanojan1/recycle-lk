{include file="common/header.tpl"}
{include file="common/top_bar.tpl"}
{include file="common/nav_bar.tpl"}

<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="#">Home</a>
                </li>
                <li>My account</li>
            </ul>

        </div>

        <div class="col-md-12">

            {include file="common/report.tpl"}
        
        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU *** -->
            {include file="common/my_account_sidebar.tpl"}
            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-4">
            <div class="box">
                <h1>Category</h1>
                <p class="lead">Add New Category</p>

                <form action="category.php?job=save&id={$category_info.id}" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="category">Category</label>
                                <input type="text" class="form-control" id="category" name="category" value="{$category_info.category}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="parent_category">Parent Category</label>
                                <select class="form-control" id="parent_category" name="parent_category">
                                    {if $category_info.parent_category}
                                        <option value="{$category_info.parent_category}">{$category_info.parent_category}</option>
                                    {else}
                                        <option disabled selected>Select Category</option>
                                    {/if}
                                    {foreach $categories as $category}
                                        {if !$category.parent_category}
                                            <option value="{$category.category}">{$category.category}</option>
                                        {/if}
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-sm-12">
                            {if $edit=="On"}
                                <button type="submit" name="submit" value="Update" class="btn btn-primary"><i class="fa fa-money"></i> Update</button>
                            {else}
                                <button type="submit" name="submit" value="Save" class="btn btn-primary"><i class="fa fa-money"></i> Save</button>
                            {/if}
                        </div>
                    </div>
                </form>  
            </div>
        </div>

        <div class="col-md-5">
            <div class="box">
                <h1>Categories</h1>
                <p class="lead">Current Categories</p>
                <div class="row">

                    <div class="table-responsive">          
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Edit</th>
                                    <th>Parent Category</th>
                                    <th>Category</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $categories as $category}
                                    <tr>
                                        <td>
                                            <a href="category.php?job=edit&id={$category.id}">
                                                <div class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></div>
                                            </a>
                                        </td>
                                        <td>{$category.parent_category}</td>
                                        <td>{$category.category}</td>
                                        <td>
                                            <a href="category.php?job=delete&id={$category.id}">
                                                <div class="btn btn-sm btn-danger"><i class="fa fa-times"></i></div>
                                            </a>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>

                </div> 
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->


{include file="common/footer.tpl"}
{include file="common/copyright.tpl"}
{include file="common/footer_js.tpl"}