{include file="common/header.tpl"}
{include file="common/top_bar.tpl"}
{include file="common/nav_bar.tpl"}

<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="#">Home</a>
                </li>
                <li>My account</li>
            </ul>

        </div>

        <div class="col-md-12">

            {include file="common/report.tpl"}
        
        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU *** -->
            {include file="common/my_account_sidebar.tpl"}
            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-4">
            <div class="box">
                <h1>Slider</h1>
                <p class="lead">Add New Slider Image</p>

                <form action="slider.php?job=save" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="slider_image">Slider Image</label>
                                <input type="file" class="form-control" id="slider_image" name="slider_image" required>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-money"></i> Save</button>
                        </div>
                    </div>
                </form>  
            </div>
        </div>

        <div class="col-md-5">
            <div class="box">
                <h1>Sliders</h1>
                <p class="lead">Current Slider Images</p>
                <div class="row">
                    {foreach $sliders as $slider}
                        <div class="item col-md-6 text-center">
                            <a href="#" class="thumb">
                                <img src="{$slider.image}" alt="" class="img-responsive">
                            </a>
                            <a href="slider.php?job=delete&id={$slider.id}">
                                <div class="btn btn-sm btn-danger"><i class="fa fa-times"> Delete</i></div>
                            </a>
                        </div>
                    {/foreach}
                </div> 
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->


{include file="common/footer.tpl"}
{include file="common/copyright.tpl"}
{include file="common/footer_js.tpl"}