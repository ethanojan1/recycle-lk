{include file="common/header.tpl"}
{include file="common/top_bar.tpl"}
{include file="common/nav_bar.tpl"}

<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="#">Home</a>
                </li>
                <li>My account</li>
            </ul>

        </div>

        <div class="col-md-12">

            {include file="common/report.tpl"}
        
        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU *** -->
            {include file="common/my_account_sidebar.tpl"}
            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-4">
            <div class="box">
                <h1>highlight</h1>
                <p class="lead">Add New highlight Image</p>

                <form action="highlights.php?job=save&id={$highlight_info.id}" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" id="title" name="title" value="{$highlight_info.title}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="text">Text</label>
                                <input type="text" class="form-control" id="text" name="text" value="{$highlight_info.text}" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="font">Font</label>
                                <input type="text" class="form-control" id="font" name="font" value="{$highlight_info.font}" required>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-sm-12">
                            {if $edit=="On"}
                                <button type="submit" name="submit" value="Update" class="btn btn-primary"><i class="fa fa-money"></i> Update</button>
                            {else}
                                <button type="submit" name="submit" value="Save" class="btn btn-primary"><i class="fa fa-money"></i> Save</button>
                            {/if}
                        </div>
                    </div>
                </form>  
            </div>
        </div>

        <div class="col-md-5">
            <div class="box">
                <h1>highlights</h1>
                <p class="lead">Current highlight Images</p>
                <div class="row">
                    {foreach $highlights as $highlight}
                        <div class="item col-md-6 text-center">
                            <i class="{$highlight.font}"></i>
                            <h4>{$highlight.title}</h4>
                            <p>{$highlight.text}</p>
                            <a href="highlights.php?job=delete&id={$highlight.id}">
                                <div class="btn btn-sm btn-danger"><i class="fa fa-times"> Delete</i></div>
                            </a>
                            <a href="highlights.php?job=edit&id={$highlight.id}">
                                <div class="btn btn-sm btn-success"><i class="fa fa-times"> Edit</i></div>
                            </a>
                        </div>
                    {/foreach}
                </div> 
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->


{include file="common/footer.tpl"}
{include file="common/copyright.tpl"}
{include file="common/footer_js.tpl"}