{include file="common/header.tpl"}
{include file="common/top_bar.tpl"}
{include file="common/nav_bar.tpl"}

<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="#">Home</a>
                </li>
                <li>My account</li>
            </ul>

        </div>

        <div class="col-md-12">

            {include file="common/report.tpl"}
        
        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU *** -->
            {include file="common/my_account_sidebar.tpl"}
            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-4">
            <div class="box">
                <h1>Label</h1>
                <p class="lead">Add New Label</p>

                <form action="label.php?job=save&id={$label_info.id}" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="label">Label</label>
                                <input type="text" class="form-control" id="label" name="label" value="{$label_info.label}" required>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-sm-12">
                            {if $edit=="On"}
                                <button type="submit" name="submit" value="Update" class="btn btn-primary"><i class="fa fa-money"></i> Update</button>
                            {else}
                                <button type="submit" name="submit" value="Save" class="btn btn-primary"><i class="fa fa-money"></i> Save</button>
                            {/if}
                        </div>
                    </div>
                </form>  
            </div>
        </div>

        <div class="col-md-5">
            <div class="box">
                <h1>Labels</h1>
                <p class="lead">Current Labels</p>
                <div class="row">

                    <div class="table-responsive">          
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Edit</th>
                                    <th>Label</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                {foreach $labels as $label}
                                    <tr>
                                        <td>
                                            <a href="label.php?job=edit&id={$label.id}">
                                                <div class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></div>
                                            </a>
                                        </td>
                                        <td>{$label.label}</td>
                                        <td>
                                            <a href="label.php?job=delete&id={$label.id}">
                                                <div class="btn btn-sm btn-danger"><i class="fa fa-times"></i></div>
                                            </a>
                                        </td>
                                    </tr>
                                {/foreach}
                            </tbody>
                        </table>
                    </div>

                </div> 
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->


{include file="common/footer.tpl"}
{include file="common/copyright.tpl"}
{include file="common/footer_js.tpl"}