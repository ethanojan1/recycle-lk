{include file="common/header.tpl"}
{include file="common/top_bar.tpl"}
{include file="common/nav_bar.tpl"}

<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="#">Home</a>
                </li>
                <li>My account</li>
            </ul>

        </div>

        <div class="col-md-12">

            {include file="common/report.tpl"}
        
        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU *** -->
            {include file="common/my_account_sidebar.tpl"}
            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-9">
            <div class="box">
                <h1>Sell Now!</h1>
                <p class="lead">Make Money out of unused stuffs. Sell them Here!</p>

                <form action="sell_now.php?job=save" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="product_name">Product Name</label>
                                <input type="text" class="form-control" id="product_name" name="product_name" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" name="description" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="sale">Sale Type</label>
                                <select class="form-control" id="sale" name="sale" onchange="priceAndSaleType(this);" required>
                                    <option disabled selected>Select Type</option>
                                    <option value="Sale">Sale</option>
                                    <option value="Gift">Gift</option>
                                    <option value="Disposal">Disposal</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input type="number" class="form-control" id="price" name="price" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="category">Product Category</label>
                                <select class="form-control" id="category" name="category" required>
                                    <option disabled selected>Select Category</option>
                                    {foreach $categories as $category}
                                        <option value="{$category.category}">{$category.category}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="sub_category">Product Category</label>
                                <select class="form-control" id="sub_category" name="sub_category" required>
                                    <option disabled selected>Select Category First</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 alert alert-warning collapse" id="price_tip">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Select Labels</label>
                                <select class="form-control multiselect" multiple="multiple" name="label[]">
                                    {foreach $labels as $label}
                                        <option value="{$label.label}">{$label.label}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="file" class="file" id="gallery" multiple name="product_image[]" required>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-money"></i> Sell Now</button>
                        </div>
                    </div>
                </form>

                
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->
{literal}
<script>
function priceAndSaleType(input) {
    var saleType = input.value;

    if (saleType == "Gift") {
        document.getElementById("price_tip").innerHTML = "You cannot set price for gift";
        document.getElementById("price").innerHTML = "0";
        document.getElementById("price").readOnly = true; 
        $(".collapse").collapse('show'); 
    } 
    else if (saleType == "Disposal") {
        document.getElementById("price_tip").innerHTML = "You have to pay the money to the collecting person for disposing items";
        $(".collapse").collapse('show'); 
    } 
    else {
        document.getElementById("price_tip").innerHTML = "No price restriction for sales";
        $(".collapse").collapse('show');
    }
}
</script>

<script>
    $(document).on('ready', function() {


        $("#gallery").fileinput({
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: false,
            showUpload: false
        });
    });
</script>

<script type="text/javascript"> 
    $(function() {
    
        $("#category").change(function() {
          $("#sub_category").load("ajax/sub_category_by_category.php?category=" + encodeURIComponent($("#category").val()));
        });
    });                 
</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".multiselect").select2();
    
  });
</script>
{/literal}

{include file="common/footer.tpl"}
{include file="common/copyright.tpl"}
{include file="common/footer_js.tpl"}