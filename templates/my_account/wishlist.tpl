{include file="common/header.tpl"}
{include file="common/top_bar.tpl"}
{include file="common/nav_bar.tpl"}

<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="#">Home</a>
                </li>
                <li>My account</li>
            </ul>

        </div>

        <div class="col-md-12">

            {include file="common/report.tpl"}
        
        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU *** -->
            {include file="common/my_account_sidebar.tpl"}
            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-9">
            <div class="box">
                <h1>Wishlist</h1>
                <div class="row">
                    {foreach $wish_products as $product}
                        <div class="item col-md-4 text-center">
                            <a href="product.php?job=view&product_id={$product.product_id}" class="thumb">
                                <img src="{$product.product_image}" alt="" class="img-responsive">
                            </a>
                            <h4>{$product.product_name}</h4>
                            <a href="wishlist.php?job=delete&product_id={$product.product_id}">
                                <div class="btn btn-sm btn-danger"><i class="fa fa-times"> Remove from Wishlist</i></div>
                            </a>
                            <a href="product.php?job=view&product_id={$product.product_id}">
                                <div class="btn btn-sm btn-success"><i class="fa fa-eye"> View Product</i></div>
                            </a>
                            {if $product.product_status=="1"}
                                <div class="btn btn-sm btn-warning"><i class="fa fa-money"> Sold</i></div>
                            {/if}
                        </div>
                    {/foreach}
                </div> 
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->


{include file="common/footer.tpl"}
{include file="common/copyright.tpl"}
{include file="common/footer_js.tpl"}