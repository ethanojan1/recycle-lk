{include file="common/header.tpl"}
{include file="common/top_bar.tpl"}
{include file="common/nav_bar.tpl"}

<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="#">Home</a>
                </li>
                <li>My account</li>
            </ul>

        </div>

        <div class="col-md-12">

            {include file="common/report.tpl"}
        
        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU *** -->
            {include file="common/my_account_sidebar.tpl"}
            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-9">
            <div class="box">
                <h1>Products</h1>
                <div class="row">
                    {foreach $products as $product}
                        <div class="item col-md-4 text-center">
                            <a href="#" class="thumb">
                                <img src="{$product.product_image}" alt="" class="img-responsive">
                            </a>
                            <h4>{$product.product_name}</h4>
                            <a href="sell_now.php?job=delete&id={$product.id}">
                                <div class="btn btn-sm btn-danger"><i class="fa fa-times"> Delete</i></div>
                            </a>
                            <a href="sell_now.php?job=edit&id={$product.id}">
                                <div class="btn btn-sm btn-success"><i class="fa fa-pencil"> Edit</i></div>
                            </a>
                            {if $product.product_status=="0"}
                                <a href="sell_now.php?job=change_status&id={$product.id}">
                                    <div class="btn btn-sm btn-warning"><i class="fa fa-money"> Sold</i></div>
                                </a>
                            {/if}
                        </div>


                    {/foreach}
                </div> 
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->


{include file="common/footer.tpl"}
{include file="common/copyright.tpl"}
{include file="common/footer_js.tpl"}