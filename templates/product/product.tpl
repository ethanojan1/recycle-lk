{include file="common/header.tpl"}
{include file="common/top_bar.tpl"}
{include file="common/nav_bar.tpl"}

<div id="content">
    <div class="container">

        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="index.php">Home</a>
                </li>
                {if $type}
                <li><a href="product.php?job=type&type={$product_info.sale}">{$product_info.sale}</a>
                </li>
                {/if}
                {if $product_info.category}
                <li><a href="product.php?job=category&category={$product_info.category}">{$product_info.category}</a>
                </li>
                {/if}
                {if $product_info.sub_category}
                <li><a href="product.php?job=category&sub_category={$product_info.sub_category}">{$product_info.sub_category}</a>
                </li>
                {/if}
                <li>{$product_info.product_name}</li>
            </ul>

        </div>

        <div class="col-md-3">
            <!-- *** MENUS AND FILTERS ***-->
            {include file="common/category_sidebar.tpl"}
        </div>

        <div class="col-md-9">

            <div class="row" id="productMain">
                <div class="col-sm-6">
                    <div id="mainImage">
                        <img src="{$product_info.product_image}" alt="" class="img-responsive">
                    </div>

                    <div class="ribbon sale">
                        <div class="theribbon">{$product_info.sale}</div>
                        <div class="ribbon-background"></div>
                    </div>
                    <!-- /.ribbon -->
                    <div class="row" id="thumbs">
                        <div class="col-xs-4">
                            <a href="{$product_info.product_image}" class="thumb">
                                <img src="{$product_info.product_image}" alt="" class="img-responsive">
                            </a>
                        </div>
                        {if $product_info.product_image2}
                            <div class="col-xs-4">
                                <a href="{$product_info.product_image2}" class="thumb">
                                    <img src="{$product_info.product_image2}" alt="" class="img-responsive">
                                </a>
                            </div>
                        {/if}
                        {if $product_info.product_image3}
                            <div class="col-xs-4">
                                <a href="{$product_info.product_image3}" class="thumb">
                                    <img src="{$product_info.product_image3}" alt="" class="img-responsive">
                                </a>
                            </div>                        
                        {/if}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box">
                        <h1 class="text-center">{$product_info.product_name}</h1>
                        <p class="goToDescription"><a href="#details" class="scroll-to">{$product_info.category} | {$product_info.sub_category} | {$product_info.label}</a>
                        </p>
                        <p class="price">Rs. {$product_info.price|number_format:2:".":","}</p>

                        <p class="text-center buttons">
                            <a class="btn btn-primary"data-toggle="collapse" data-target="#phone"><i class="fa fa-phone"></i> View Phone No</a> 
                            <a href="whishlist.php?job=add&product_id={$product_info.product_id}" class="btn btn-default"><i class="fa fa-heart"></i> Add to wishlist</a>
                        </p>

                        <h2 id="phone" class="collapse text-center">
                            <i class="fa fa-phone"></i> {$seller_info.phone}
                        </h2> 
                    </div>
                </div>

            </div>


            <div class="box" id="details">
                
                    <h4>Product details</h4>
                    <p>{$product_info.description}</p>

                    <hr>
                    <div class="">
                        <h4>Show it to your friends</h4>
                        
                            <a href="{$link}" onclick="return popitup('https://www.facebook.com/sharer/sharer.php?u={$link}')" class="btn btn-social btn-facebook" data-animate-hover="pulse">
                                <i class="fa fa-facebook"></i> Share
                            </a>


                            <a href="{$link}" onclick="return popitup('https://twitter.com/intent/tweet?url={$link}&text={$page|escape:quotes}&via=Tanojan')" class="btn btn-social btn-twitter" data-animate-hover="pulse">
                                <i class="fa fa-twitter"></i> Tweet
                            </a>
                    </div>
            </div>

            <div class="row same-height-row">
                {include file="common/similar_products.tpl"}
            </div>

        </div>
        <!-- /.col-md-9 -->
    </div>
    <!-- /.container -->
</div>
<!-- /#content -->

{literal}
<script>
function popitup(url) {
        newwindow=window.open(url,'name','height=400,width=600');
        if (window.focus) {newwindow.focus()}
        return false;
    }
</script>
{/literal}

{include file="common/footer.tpl"}
{include file="common/copyright.tpl"}
{include file="common/footer_js.tpl"}