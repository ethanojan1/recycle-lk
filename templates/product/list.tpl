{include file="common/header.tpl"}
{include file="common/top_bar.tpl"}
{include file="common/nav_bar.tpl"}

<div id="content">
    <div class="container">

        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="index.php">Home</a>
                </li>
                {if $type}
                <li><a href="product.php?job=type&type={$type}">{$type}</a>
                </li>
                {/if}
                {if $category_info}
                <li><a href="product.php?job=category&category={$category_info}">{$category_info}</a>
                </li>
                {/if}
                {if $sub_category_info}
                <li><a href="product.php?job=sub_category&sub_category={$sub_category_info}">{$sub_category_info}</a>
                </li>
                {/if}
            </ul>

        </div>

        <div class="col-md-3">
            <!-- *** MENUS AND FILTERS ***-->
            {include file="common/category_sidebar.tpl"}
        </div>

        <div class="col-md-9">
            {include file="common/pagination_product_count.tpl"}
            

            <div class="row products">

                {foreach $products as $product}
                    <div class="col-md-4 col-sm-6">
                        <div class="product">
                            <div class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <a href="product.php?job=view&product_id={$product.product_id}">
                                            <img src="{$product.product_image}" alt="" class="img-responsive" style="width: 100%;height: 200px;">
                                        </a>
                                    </div>
                                    <div class="back">
                                        <a href="product.php?job=view&product_id={$product.product_id}">
                                            <img src="{$product.product_image}" alt="" class="img-responsive" style="width: 100%;height: 200px;">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a href="product.php?job=view&product_id={$product.product_id}" class="invisible">
                                <img src="{$product.product_image}" alt="" class="img-responsive" style="width: 100%; height: 200px;">
                            </a>
                            <div class="text">
                                <h3><a href="product.php?job=view&product_id={$product.product_id}">{$product.product_name}</a></h3>
                                <p class="price">Rs. {$product.price|number_format:2:".":","}</p>
                                <p class="buttons">
                                    <a href="detail.html" class="btn btn-success">View detail</a>
                                    
                                </p>
                            </div>
                            <!-- /.text -->
                            {if $product.sale}
                            <div class="ribbon sale">
                                <div class="theribbon">{$product.sale}</div>
                                <div class="ribbon-background"></div>
                            </div>
                            {/if}
                        </div>
                        <!-- /.product -->
                    </div>
                {/foreach}

            </div>

            {include file="common/pagination.tpl"}


        </div>
        <!-- /.col-md-9 -->
    </div>
    <!-- /.container -->
</div>
<!-- /#content -->

<!-- Load Facebook SDK for JavaScript -->
  

{include file="common/footer.tpl"}
{include file="common/copyright.tpl"}
{include file="common/footer_js.tpl"}