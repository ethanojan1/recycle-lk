{include file="common/header.tpl"}
{include file="common/top_bar.tpl"}
{include file="common/nav_bar.tpl"}

<div id="content">

    <div class="container">
        <div class="col-md-12">
            <div id="main-slider">
                {foreach $sliders as $slider}
                    <div class="item slider_adjust">
                        <img src="{$slider.image}" alt="" class="img-responsive">
                    </div>
                {/foreach}
            </div>
            <!-- /#main-slider -->
        </div>
    </div>

    <!-- *** ADVANTAGES HOMEPAGE *** -->
    <div id="advantages">

        
    </div>
    <!-- /#advantages -->

    <!-- *** ADVANTAGES END *** -->

    <!-- *** HOT PRODUCT SLIDESHOW ***
_________________________________________________________ -->
    <div id="hot">

        <div class="box padding0" >
            <div class="container">
                <div class="col-md-12">
                    <h1 class="text-center">Hot this week</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="product-slider">
                {foreach $products as $product}

                <div class="item">
                    <div class="product">
                        <div class="flip-container">
                            <div class="flipper">
                                <div class="front">
                                    <a href="product.php?job=view&product_id={$product.product_id}">
                                        <img src="{$product.product_image}" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                                <div class="back">
                                    <a href="product.php?job=view&product_id={$product.product_id}">
                                        <img src="{$product.product_image}" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <a href="product.php?job=view&product_id={$product.product_id}" class="invisible">
                            <img src="{$product.product_image}" alt="" class="img-responsive product-image-home">
                        </a>
                        <div class="text">
                            <h3><a href="product.php?job=view&product_id={$product.product_id}">{$product.product_name}</a></h3>
                            <p class="price">Rs. {$product.price|number_format:2:".":","}</p>
                        </div>
                        <!-- /.text -->
                    </div>
                    <!-- /.product -->
                </div>
                {/foreach}

            </div>
            <!-- /.product-slider -->
        </div>
        <!-- /.container -->

    </div>

    <div id="hot">

        <div class="box padding0">
            <div class="container">
                <div class="col-md-12">
                    <h1 class="text-center">Products for Sale</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="product-slider">
                {foreach $sales as $sale}

                <div class="item">
                    <div class="product">
                        <div class="flip-container">
                            <div class="flipper">
                                <div class="front">
                                    <a href="product.php?job=view&product_id={$sale.product_id}">
                                        <img src="{$sale.product_image}" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                                <div class="back">
                                    <a href="product.php?job=view&product_id={$sale.product_id}">
                                        <img src="{$sale.product_image}" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <a href="product.php?job=view&product_id={$sale.product_id}" class="invisible">
                            <img src="{$sale.product_image}" alt="" class="img-responsive product-image-home">
                        </a>
                        <div class="text">
                            <h3><a href="product.php?job=view&product_id={$sale.product_id}">{$sale.product_name}</a></h3>
                            <p class="price">Rs. {$sale.price|number_format:2:".":","}</p>
                        </div>
                        <!-- /.text -->
                    </div>
                    <!-- /.product -->
                </div>
                {/foreach}

            </div>
            <!-- /.product-slider -->
        </div>
        <!-- /.container -->

    </div>
    <!-- /#hot -->

    <div id="hot">

        <div class="box padding0">
            <div class="container">
                <div class="col-md-12">
                    <h1 class="text-center">Products as Gifts</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="product-slider">
                {foreach $gifts as $gift}

                <div class="item">
                    <div class="product">
                        <div class="flip-container">
                            <div class="flipper">
                                <div class="front">
                                    <a href="product.php?job=view&product_id={$gift.product_id}">
                                        <img src="{$gift.product_image}" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                                <div class="back">
                                    <a href="product.php?job=view&product_id={$gift.product_id}">
                                        <img src="{$gift.product_image}" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <a href="product.php?job=view&product_id={$gift.product_id}" class="invisible">
                            <img src="{$gift.product_image}" alt="" class="img-responsive product-image-home">
                        </a>
                        <div class="text">
                            <h3><a href="product.php?job=view&product_id={$gift.product_id}">{$gift.product_name}</a></h3>
                            <p class="price">Rs. {$gift.price|number_format:2:".":","}</p>
                        </div>
                        <!-- /.text -->
                    </div>
                    <!-- /.product -->
                </div>
                {/foreach}

            </div>
            <!-- /.product-slider -->
        </div>
        <!-- /.container -->

    </div>
    <!-- /#hot -->

    <div id="hot">

        <div class="box padding0">
            <div class="container">
                <div class="col-md-12">
                    <h1 class="text-center">Products for Disposal</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="product-slider">
                {foreach $disposals as $disposal}

                <div class="item">
                    <div class="product">
                        <div class="flip-container">
                            <div class="flipper">
                                <div class="front">
                                    <a href="product.php?job=view&product_id={$disposal.product_id}">
                                        <img src="{$disposal.product_image}" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                                <div class="back">
                                    <a href="product.php?job=view&product_id={$disposal.product_id}">
                                        <img src="{$disposal.product_image}" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <a href="product.php?job=view&product_id={$disposal.product_id}" class="invisible">
                            <img src="{$disposal.product_image}" alt="" class="img-responsive product-image-home">
                        </a>
                        <div class="text">
                            <h3><a href="product.php?job=view&product_id={$disposal.product_id}">{$disposal.product_name}</a></h3>
                            <p class="price">Rs. {$disposal.price|number_format:2:".":","}</p>
                        </div>
                        <!-- /.text -->
                    </div>
                    <!-- /.product -->
                </div>
                {/foreach}

            </div>
            <!-- /.product-slider -->
        </div>
        <!-- /.container -->

    </div>
    <!-- /#hot -->

    <!-- *** HOT END *** -->

   

</div>
<!-- /#content -->

{include file="common/footer.tpl"}
{include file="common/copyright.tpl"}
{include file="common/footer_js.tpl"}