<div id="copyright">
    <div class="container">
        <div class="col-md-6">
            <p class="pull-left">© {'Y'|date}, Recycle.lk</p>

        </div>
        <div class="col-md-6">
            <p class="pull-right">Developed by <a href="https://thekoders.com">The Koder</a>
                 <!-- Not removing these links is part of the license conditions of the template. Thanks for understanding :) If you want to use the template without the attribution links, you can do so after supporting further themes development at https://bootstrapious.com/donate  -->
            </p>
        </div>
    </div>
</div>