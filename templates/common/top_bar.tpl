<div id="top">
    <div class="container">
        <div class="col-md-6 offer" data-animate="fadeInDown">
            {if $last_disposal}
                <a href="product.php?job=type&type=Disposal" class="btn btn-danger btn-sm" data-animate-hover="shake">
                    Hot: Disposing product
                </a>
                <a href="product.php?job=view&product_id={$last_disposal.product_id}">
                   {$last_disposal.category} | {$last_disposal.product_name} | Rs. {$last_disposal.price|number_format:2:".":","}
                </a>
            {/if}
        </div>
        <div class="col-md-6" data-animate="fadeInDown">
            <ul class="menu">
                {if $login==1}
                    <li><a href="my_account.php?job=profile" class="btn btn-danger btn-sm">{$first_name}</a> 
                    </li>
                    <li><a href="my_account.php?job=profile">My Account</a> 
                    </li>
                    <li><a href="login.php?job=logout">Logout</a>
                    </li>
                {else}
                    <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a>
                    </li>
                    <li><a href="register.php?job=register">Register</a>
                    </li>
                {/if}     
            </ul>
        </div>
    </div>
    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
        <div class="modal-dialog modal-sm">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="Login">Customer login</h4>
                </div>
                <div class="modal-body">
                    <form action="login.php?job=login" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" id="email-modal" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password-modal" name="password" placeholder="Password">
                        </div>

                        <p class="text-center">
                            <button class="btn btn-primary"><i class="fa fa-sign-in"></i> Log in</button>
                        </p>

                    </form>

                    <p class="text-center text-muted">Not registered yet?</p>
                    <p class="text-center text-muted"><a href="register.php?job=register"><strong>Register now</strong></a>! It is easy and done in 1&nbsp;minute and gives you access to much more!</p>

                </div>
            </div>
        </div>
    </div>

</div>