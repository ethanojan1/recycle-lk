<div class="box info-bar">
    <div class="row">
        <div class="col-sm-12 col-md-12 products-showing">
            <p> Displaying <strong>{$range}</strong> products out of <strong>{$total}</strong> products</p>
        </div>
    </div>
</div>