<div class="col-md-3 col-sm-6">
    <div class="box same-height green">
        <h3>You may also like these products</h3>
    </div>
</div>

{foreach $similar_products as $similar_product}

    {if $similar_product.product_id != $product_info.product_id}

        <div class="col-md-3 col-sm-6">
            <div class="product same-height">
                <div class="flip-container">
                    <div class="flipper">
                        <div class="front">
                            <a href="product.php?job=view&product_id={$similar_product.product_id}">
                                <img src="{$similar_product.product_image}" alt="" class="img-responsive product-image-home">
                            </a>
                        </div>
                        <div class="back">
                            <a href="product.php?job=view&product_id={$similar_product.product_id}">
                                <img src="{$similar_product.product_image}" alt="" class="img-responsive product-image-home">
                            </a>
                        </div>
                    </div>
                </div>
                <a href="product.php?job=view&product_id={$similar_product.product_id}" class="invisible">
                    <img src="{$similar_product.product_image}" alt="" class="img-responsive product-image-home">
                </a>
                <div class="text">
                    <h3><a href="product.php?job=view&product_id={$similar_product.product_id}">{$similar_product.product_name}</a></h3>
                    <p class="price">Rs. {$similar_product.price|number_format:2:".":","}</p>
                </div>
            </div>
            <!-- /.product -->
        </div>
    {/if}

{/foreach}