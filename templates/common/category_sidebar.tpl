<div class="panel panel-default sidebar-menu">

    <div class="panel-heading">
        <h3 class="panel-title">Categories</h3>
    </div>

    <div class="panel-body">
        <ul class="nav nav-pills nav-stacked category-menu"> 
            {foreach $categories as $category}
                <li {if $product_info.category==$category.category OR $product_info.category==$category.category OR $page==$category.category} class="active" {/if}>
                    <a href="product.php?job=category&category={$category.category}&sub_category=no">{$category.category}</a>
                    <ul>
                        {foreach $sub_categories as $sub_category}
                            {if $sub_category.parent_category==$category.category}
                                <li><a href="product.php?job=category&category={$category.category}&sub_category={$sub_category.category}"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i> {$sub_category.category}</a></li>
                            {/if}
                        {/foreach}
                    </ul>
                </li>

            {/foreach}
        </ul>

    </div>
</div>