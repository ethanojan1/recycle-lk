{if $report=="error"}
    <div class="alert alert-danger">
    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <h4>Error!</h4>
        <hr>
        {$message}
    </div>
    
{elseif $report=="success"}
    <div class="alert alert-success">
    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <h4>Success!</h4>
        <hr>
        {$message}
    </div>
{/if}