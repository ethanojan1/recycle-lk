<div class="pages">
    <ul class="pagination">
        {if $current_page_no>1}
        <li>
        <a href="product.php?job=pagination&page_no={$prev}">&laquo;</a>
        </li>
        {/if}
        
        {foreach $page_nos as $page_no}
        <li {if $current_page_no==$page_no} class="active" {/if} >
            <a href="product.php?job=pagination&page_no={$page_no}">
                {$page_no}
            </a>
        </li>
        {/foreach}

        {if $current_page_no!=$max_page_no}
        <li>
        <a href="product.php?job=pagination&page_no={$next}">&raquo;</a>
        </li>
        {/if}
    </ul>
</div>