<div class="panel panel-default sidebar-menu">

    <div class="panel-heading">
        <h3 class="panel-title">Customer section</h3>
    </div>

    <div class="panel-body">

        <ul class="nav nav-pills nav-stacked">

            <li {if $page=="My Profile"}class="active"{/if}>
                <a href="my_account.php?job=profile"><i class="fa fa-user"></i> My account</a>
            </li>
            <li {if $page=="Sell Now"}class="active"{/if}>
                <a href="sell_now.php?job=sell"><i class="fa fa-money"></i> Sell Now</a>
            </li>
            <li {if $page=="My Products"}class="active"{/if}>
                <a href="sell_now.php?job=view"><i class="fa fa-money"></i> My Products</a>
            </li>
            <li>
                <a href="customer-wishlist.html"><i class="fa fa-heart"></i> My wishlist</a>
            </li>
            <li>
                <a href="index.html"><i class="fa fa-sign-out"></i> Logout</a>
            </li>
        </ul>
    </div>

    {if $admin==1}
        <div class="panel-heading">
            <h3 class="panel-title">Admin section</h3>
        </div>

        <div class="panel-body">

            <ul class="nav nav-pills nav-stacked">

                <li {if $page=="Slider"}class="active"{/if}>
                    <a href="slider.php?job=slider"><i class="fa fa-money"></i> Slider</a>
                </li>
                <li {if $page=="Highlight"}class="active"{/if}>
                    <a href="highlights.php?job=highlight"><i class="fa fa-user"></i> Highlight</a>
                </li>
                <li {if $page=="Category"}class="active"{/if}>
                    <a href="category.php?job=category"><i class="fa fa-list"></i> Category</a>
                </li>
                <li {if $page=="Label"}class="active"{/if}>
                    <a href="label.php?job=label"><i class="fa fa-list"></i> Label</a>
                </li>
            </ul>
        </div>
    {/if}

</div>