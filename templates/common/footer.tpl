<div id="footer" data-animate="fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <h4>Pages</h4>

                <ul>
                    <li><a href="about.php">About us</a>
                    </li>
                    <li><a href="term.php">Terms and conditions</a>
                    </li>
                    <li><a href="faq.php">FAQ</a>
                    </li>
                    <li><a href="contact.php">Contact us</a>
                    </li>
                </ul>

                <hr class="hidden-md hidden-lg hidden-sm">

            </div>
            <!-- /.col-md-3 -->

            <div class="col-md-3 col-sm-6">

                <h4>Products Type</h4>

                <ul>
                    <li><a href="product.php?job=type&type=Sale">Sale</a>
                    </li>
                    <li><a href="product.php?job=type&type=Gift">Gift</a>
                    </li>
                    <li><a href="product.php?job=type&type=Sale">Disposal</a>
                    </li>
                </ul>

                <hr class="hidden-md hidden-lg">

            </div>
            <!-- /.col-md-3 -->

            <div class="col-md-3 col-sm-6">

                <h4>Get the news</h4>

                <p class="text-muted">Subscribe now and get all the important news about us.</p>

                <form action="register.php?job=subscribe" method="POST">
                    <div class="input-group">

                        <input type="email" name="email" class="form-control" required>

                        <span class="input-group-btn">
                            <button class="btn btn-success" type="submit">Subscribe!</button>
                        </span>

                    </div>
                    <!-- /input-group -->
                </form>

                <hr class="hidden-md hidden-lg">

            </div>
            <!-- /.col-md-3 -->

            <div class="col-md-3 col-sm-6">

                <h4>Stay in touch</h4>

                <p class="social">
                    <a href="#" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="twitter external" data-animate-hover="shake"><i class="fa fa-twitter"></i></a>
                </p>


            </div>
            <!-- /.col-md-3 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
</div>