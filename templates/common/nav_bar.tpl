<div class="navbar navbar-default yamm" role="navigation" id="navbar">
    <div class="container">
        <div class="navbar-header">

            <a class="navbar-brand home" href="index.php" data-animate-hover="bounce">
                <img src="img/logo.png" alt="Recycle.LK" class="hidden-xs">
                <img src="img/logo.png" alt="Recycle.LK" class="visible-xs"><span class="sr-only">Recycle.LK</span>
            </a>
            <div class="navbar-buttons">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-align-justify"></i>
                </button>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                    <span class="sr-only">Toggle search</span>
                    <i class="fa fa-search"></i>
                </button>
                <a class="btn btn-default navbar-toggle" href="basket.html">
                    <i class="fa fa-shopping-cart"></i>  <span class="hidden-xs">3 items in cart</span>
                </a>
            </div>
        </div>
        <!--/.navbar-header -->

        <div class="navbar-collapse collapse" id="navigation">

            <ul class="nav navbar-nav navbar-left">
                <li {if $page=="Home"} class="active" {/if}>
                    <a href="index.php">Home</a>
                </li>
                <li {if $type=="Sale"} class="active" {/if}>
                    <a href="product.php?job=type&type=Sale">Sales</a>
                </li>

                <li {if $type=="Gift"} class="active" {/if}>
                    <a href="product.php?job=type&type=Gift">Gift</a>
                </li>

                <li {if $type=="Disposal"} class="active" {/if}>
                    <a href="product.php?job=type&type=Disposal">Disposal</a>
                </li>
            </ul>

        </div>
        <!--/.nav-collapse -->

        <div class="navbar-buttons">

            <div class="navbar-collapse collapse right" id="search-not-mobile">
                <form class="navbar-form" role="search" action="product.php?job=search" method="POST">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search" required>
                        <span class="input-group-btn">

                            <button type="submit" class="btn btn-primary btn-special"><i class="fa fa-search"></i></button>

                        </span>
                    </div>
                </form>
            </div>

        </div>
        <!--/.nav-collapse -->

    </div>
    <!-- /.container -->
</div>
<!-- /#navbar -->
