<?php
require_once 'conf/smarty-conf.php';
include 'functions/common_functions.php';

$sales=get_data('inventory', 'sale', 'Sale', '10');
$gifts=get_data('inventory', 'sale', 'Gift', '10');
$disposals=get_data('inventory', 'sale', 'disposal', '10');
$products=get_data('inventory', '', '', '10');
$sliders=get_data('slider', '', '', '5');
$highlights=get_data('highlights', '', '', '3');

$smarty->assign('sales', $sales);
$smarty->assign('gifts', $gifts);
$smarty->assign('disposals', $disposals);
$smarty->assign('last_disposal', $disposals[0]);
$smarty->assign('products', $products);
$smarty->assign('sliders', $sliders);
$smarty->assign('highlights', $highlights);
$smarty->assign('page',"Home");
$smarty->display('index.tpl');