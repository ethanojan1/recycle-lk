<?php
require_once 'conf/smarty-conf.php';
include 'functions/common_functions.php';

if($_SESSION['login']==1){
	//defaults for the file
	$table="slider";
	$condition_field="id";
	$condition_value=$_REQUEST['id'];

	if($_SESSION['role']=="Admin"){

		if($_REQUEST['job']=="slider"){
			$sliders=get_data('slider', '', '', '');

			$smarty->assign('sliders', $sliders);
			$smarty->assign('page',"Slider");
			$smarty->display('my_account/admin/slider.tpl');
		}

		elseif($_REQUEST['job']=="save"){
			$filename = stripslashes ($_FILES['slider_image'] ['name']);
			$file_name=$filename;
			$slider_image="slider_image/".$file_name;
			$copied = copy($_FILES['slider_image']['tmp_name'], $slider_image);

			$data = array('image' => $slider_image);

			save_data($table, $data);
			
			$sliders=get_data('slider', '', '', '');

			$smarty->assign('sliders', $sliders);
			$smarty->assign('page',"Slider");
			$smarty->display('my_account/admin/slider.tpl');
		}

		elseif($_REQUEST['job']=="delete"){
			
			$data = array('cancel_status' => 1);

			update_data($table, $data, $condition_field, $condition_value);
			
			$sliders=get_data('slider', '', '', '');

			$smarty->assign('sliders', $sliders);
			$smarty->assign('page',"Slider");
			$smarty->display('my_account/admin/slider.tpl');
		}

		else{
			$sliders=get_data('slider', '', '', '');

			$smarty->assign('sliders', $sliders);
			$smarty->assign('page',"Slider");
			$smarty->display('my_account/admin/slider.tpl');
		}
	}
	else{
		$smarty->assign('report',"error");
		$smarty->assign('message',"You don't have admin permission.");
		$smarty->display('my_account/profile.tpl');
	}
}

else{
	$smarty->assign('page',"Login");
	$smarty->display('register/register.tpl');	
}
