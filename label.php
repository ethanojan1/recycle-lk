<?php
require_once 'conf/smarty-conf.php';
include 'functions/common_functions.php';

if($_SESSION['login']==1){
	//defaults for the file
	$table="label";
	$condition_field="id";
	$condition_value=$_REQUEST['id'];

	if($_SESSION['role']=="Admin"){

		if($_REQUEST['job']=="label"){
			$labels=get_data('label', '', '', '');

			$smarty->assign('labels', $labels);
			$smarty->assign('page',"Label");
			$smarty->display('my_account/admin/label.tpl');
		}

		elseif($_REQUEST['job']=="save"){
			$data = array('label' => $_POST['label']
				);

			if($_REQUEST['submit']=="Save"){
				save_data($table, $data);
			}
			else{
				update_data($table, $data, $condition_field, $condition_value);
			}
			
			$labels=get_data('label', '', '', '');

			$smarty->assign('labels', $labels);
			$smarty->assign('page',"Label");
			$smarty->display('my_account/admin/label.tpl');
		}

		elseif($_REQUEST['job']=="edit"){
			$label_info=get_data($table, $condition_field, $condition_value, '1');
			
			$labels=get_data('label', '', '', '');

			$smarty->assign('edit', "On");
			$smarty->assign('label_info', $label_info);
			$smarty->assign('labels', $labels);
			$smarty->assign('page',"Label");
			$smarty->display('my_account/admin/label.tpl');
		}


		elseif($_REQUEST['job']=="delete"){
			
			$data = array('cancel_status' => 1);

			update_data($table, $data, $condition_field, $condition_value);
			
			$labels=get_data('label', '', '', '');

			$smarty->assign('labels', $labels);
			$smarty->assign('page',"Label");
			$smarty->display('my_account/admin/label.tpl');
		}

		else{
			$labels=get_data('label', '', '', '');

			$smarty->assign('labels', $labels);
			$smarty->assign('page',"Label");
			$smarty->display('my_account/admin/label.tpl');
		}
	}
	else{
		$smarty->assign('report',"error");
		$smarty->assign('message',"You don't have admin permission.");
		$smarty->display('my_account/profile.tpl');
	}
}

else{
	$smarty->assign('page',"Login");
	$smarty->display('register/register.tpl');	
}
	