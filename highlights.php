<?php
require_once 'conf/smarty-conf.php';
include 'functions/common_functions.php';

if($_SESSION['login']==1){
	//defaults for the file
	$table="highlights";
	$condition_field="id";
	$condition_value=$_REQUEST['id'];

	if($_SESSION['role']=="Admin"){

		if($_REQUEST['job']=="highlight"){
			$highlights=get_data('highlights', '', '', '');

			$smarty->assign('highlights', $highlights);
			$smarty->assign('page',"Highlight");
			$smarty->display('my_account/admin/highlight.tpl');
		}

		elseif($_REQUEST['job']=="save"){
			$data = array('title' => $_POST['title'],
				'text' => $_POST['text'],
				'font' => $_POST['font']
				);

			if($_REQUEST['submit']=="Save"){
				save_data($table, $data);
			}
			else{
				update_data($table, $data, $condition_field, $condition_value);
			}
			
			$highlights=get_data('highlights', '', '', '');

			$smarty->assign('highlights', $highlights);
			$smarty->assign('page',"Highlight");
			$smarty->display('my_account/admin/highlight.tpl');
		}

		elseif($_REQUEST['job']=="edit"){
			$highlight_info=get_data($table, $condition_field, $condition_value, '1');
			
			$highlights=get_data('highlights', '', '', '');

			$smarty->assign('edit', "On");
			$smarty->assign('highlights', $highlights);
			$smarty->assign('highlight_info', $highlight_info);
			$smarty->assign('page',"Highlight");
			$smarty->display('my_account/admin/highlight.tpl');
		}

		elseif($_REQUEST['job']=="delete"){
			
			$data = array('cancel_status' => 1);

			update_data($table, $data, $condition_field, $condition_value);
			
			$highlights=get_data('highlights', '', '', '');

			$smarty->assign('highlights', $highlights);
			$smarty->assign('page',"Highlight");
			$smarty->display('my_account/admin/highlight.tpl');
		}

		else{
			$highlights=get_data('highlights', '', '', '');

			$smarty->assign('highlights', $highlights);
			$smarty->assign('page',"Highlight");
			$smarty->display('my_account/admin/highlight.tpl');
		}
	}
	else{
		$smarty->assign('report',"error");
		$smarty->assign('message',"You don't have admin permission.");
		$smarty->display('my_account/profile.tpl');
	}
}

else{
	$smarty->assign('page',"Login");
	$smarty->display('register/register.tpl');	
}
