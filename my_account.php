<?php
require_once 'conf/smarty-conf.php';
include 'functions/common_functions.php';

if($_SESSION['login']==1){
	//defaults for the file
	$table="public_users";
	$condition_field="user_id";
	$condition_value=$_SESSION['user_id'];

	if($_REQUEST['job']=="password"){

		$password_old=md5($_POST['password_old']);
		$password_1=md5($_POST['password_1']);
		$password_2=md5($_POST['password_2']);

		$user_info=get_data($table, $condition_field, $condition_value, '1');

		$password_in_db=$user_info['password'];

		if($password_in_db==$password_old){
			if($password_1==$password_2){

				$data = array('password' => $password_1);

				update_data($table, $data, $condition_field, $condition_value);
			}
			else{
				$smarty->assign('report',"error");
				$smarty->assign('message',"Both Password must be same!");
			}
		}
		else{
			$smarty->assign('report',"error");
			$smarty->assign('message',"Old password you entered is wrong!");
		}



		$smarty->assign('page',"My Profile");
		$smarty->display('my_account/profile.tpl');
	}

	elseif($_REQUEST['job']=="edit_profile"){
		$table="public_users";

		$data = array('first_name' => $_POST['first_name'], 
			'last_name'=> $_POST['last_name'],
			'address'=> $_POST['address'],
			'zip'=> $_POST['zip'],
			'district'=> $_POST['district'],
			'phone'=> $_POST['phone'],
			'email'=> $_POST['email']
		);

		$condition_field="user_id";
		$condition_value=$_SESSION['user_id'];

		update_data($table, $data, $condition_field, $condition_value);
		$user_info=get_data($table, $condition_field, $condition_value, '1');

		$smarty->assign('user_info', $user_info);
		$smarty->assign('page',"My Profile");
		$smarty->display('my_account/profile.tpl');
	}

	else{
		$user_info=get_data($table, $condition_field, $condition_value, '1');

		$smarty->assign('user_info', $user_info);
		$smarty->assign('page',"My Profile");
		$smarty->display('my_account/profile.tpl');
	}
}

else{
	$smarty->assign('page',"Login");
	$smarty->display('register/register.tpl');	
}
