<?php
require_once 'conf/smarty-conf.php';
include 'functions/common_functions.php';

//defaults for the file
$table="inventory";
$condition_field="product_id";
$condition_value=$_REQUEST['product_id'];

if($_REQUEST['job']=="view"){
	$product_info=get_data($table, $condition_field, $condition_value, '1');
	$similar_products=get_data($table, 'category', $product_info['category'], '3');
	$seller_info=get_data('public_users', 'user_id', $product_info['user_id'], '1');
	$categories=get_data('category', 'parent_category', '', '');
	$sub_categories=get_data('category', '', '', '');

	$smarty->assign('categories', $categories);
	$smarty->assign('sub_categories', $sub_categories);
	$smarty->assign('product_info', $product_info);
	$smarty->assign('seller_info', $seller_info);
	$smarty->assign('similar_products', $similar_products);
	$smarty->assign('page', $product_info['product_name']);
	$smarty->display('product/product.tpl');
}
elseif($_REQUEST['job']=="search"){
	
	$search_value=$_POST['search'];		
	
	$_SESSION['condition_without_limit']="WHERE (product_name LIKE '%$search_value%' OR category LIKE '%$search_value%' OR sub_category LIKE '%$search_value%' OR description LIKE '%$search_value%') AND cancel_status = '0' AND product_status = '0' ORDER BY 'id' DESC";

	$condition = $_SESSION['condition_without_limit']." LIMIT 0, $display_limit";

	$products=get_paginated_data ($table, $condition);
	$total=get_data_count ($table, $_SESSION['condition_without_limit']);
	$total_page_no=ceil($total / $display_limit);

	//pagination//

	$pagination_data=pagination($page_no, $total, $total_page_no, $limit, $display_limit);

	$columns = array_keys ( $pagination_data );
	$values = array_values ( $pagination_data );

	for($i = 0; $i < count ( $pagination_data ); $i ++) {
		$smarty->assign($columns[$i], $values[$i]);
	}
	//pagination end//

	$categories=get_data('category', 'parent_category', '', '');
	$sub_categories=get_data('category', '', '', '');
	
	$smarty->assign('type', $_REQUEST['type']);
	$smarty->assign('categories', $categories);
	$smarty->assign('sub_categories', $sub_categories);
	$smarty->assign('products', $products);
	$smarty->assign('page', $_REQUEST['type']);
	$smarty->display('product/list.tpl');
}

elseif($_REQUEST['job']=="type"){
	$condition_field="";
	$type=$_REQUEST['type'];		
	
	$_SESSION['condition_without_limit']="WHERE sale='$type' AND cancel_status = '0' AND product_status = '0' ORDER BY 'id' DESC";

	$condition = $_SESSION['condition_without_limit']." LIMIT 0, $display_limit";

	$products=get_paginated_data ($table, $condition);
	$total=get_data_count ($table, $_SESSION['condition_without_limit']);
	$total_page_no=ceil($total / $display_limit);

	//pagination//

	$pagination_data=pagination($page_no, $total, $total_page_no, $limit, $display_limit);

	$columns = array_keys ( $pagination_data );
	$values = array_values ( $pagination_data );

	for($i = 0; $i < count ( $pagination_data ); $i ++) {
		$smarty->assign($columns[$i], $values[$i]);
	}
	//pagination end//

	$categories=get_data('category', 'parent_category', '', '');
	$sub_categories=get_data('category', '', '', '');
	
	$smarty->assign('type', $_REQUEST['type']);
	$smarty->assign('categories', $categories);
	$smarty->assign('sub_categories', $sub_categories);
	$smarty->assign('products', $products);
	$smarty->assign('page', $_REQUEST['type']);
	$smarty->display('product/list.tpl');
}

elseif($_REQUEST['job']=="category"){
	if(!$_REQUEST['sub_category'] OR $_REQUEST['sub_category']=="no"){
		$condition_field="category";
		$condition_value=$_REQUEST['category'];
	}
	else{
		$condition_field="sub_category";
		$condition_value=$_REQUEST['sub_category'];		
	}
	
	$_SESSION['condition_without_limit'] = "WHERE $condition_field='$condition_value' AND cancel_status = '0' AND product_status = '0' ORDER BY 'id' DESC ";

	$condition = $_SESSION['condition_without_limit']." LIMIT 0, $display_limit";

	$products=get_paginated_data ($table, $condition);
	$total=get_data_count ($table, " WHERE $condition_field='$condition_value' AND cancel_status = '0' AND product_status = '0'");
	$total_page_no=ceil($total / $display_limit);

	//pagination//

	$pagination_data=pagination($page_no, $total, $total_page_no, $limit, $display_limit);

	$columns = array_keys ( $pagination_data );
	$values = array_values ( $pagination_data );

	for($i = 0; $i < count ( $pagination_data ); $i ++) {
		$smarty->assign($columns[$i], $values[$i]);
	}
	//pagination end//

	$categories=get_data('category', 'parent_category', '', '');
	$sub_categories=get_data('category', '', '', '');
	
	$smarty->assign('category_info', $_REQUEST['category']);
	$smarty->assign('categories', $categories);
	$smarty->assign('sub_categories', $sub_categories);
	$smarty->assign('products', $products);
	$smarty->assign('page', $_REQUEST['category']);
	$smarty->display('product/list.tpl');
}

elseif($_REQUEST['job']=="pagination"){
	$page_no=$_REQUEST['page_no'];
	$limit=($page_no-1)*$display_limit;

	$condition = $_SESSION['condition_without_limit']." LIMIT $limit, $display_limit";

	$products=get_paginated_data ($table, $condition);
	$total=get_data_count ($table, $_SESSION['condition_without_limit']);
	$total_page_no=ceil($total / $display_limit);

	//pagination//

	$pagination_data=pagination($page_no, $total, $total_page_no, $limit, $display_limit);

	$columns = array_keys ( $pagination_data );
	$values = array_values ( $pagination_data );

	for($i = 0; $i < count ( $pagination_data ); $i ++) {
		$smarty->assign($columns[$i], $values[$i]);
	}
	//pagination end//

	$categories=get_data('category', 'parent_category', '', '');
	$sub_categories=get_data('category', '', '', '');
	
	$smarty->assign('category_info', $_REQUEST['category']);
	$smarty->assign('categories', $categories);
	$smarty->assign('sub_categories', $sub_categories);
	$smarty->assign('products', $products);
	$smarty->assign('page', $_REQUEST['category']);
	$smarty->display('product/list.tpl');
}

else{
	$product_info=get_data('inventory', '', '', '1');

	$smarty->assign('product_info', $product_info);
	$smarty->assign('page', $product_info['product_name']);
	$smarty->display('product/product.tpl');
}
