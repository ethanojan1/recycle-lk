<?php

function get_data($table, $condition_field, $condition_value, $limit) {
	include 'conf/config.php';
	include 'conf/opendb.php';

	if (!$condition_field){
		$condition = "";
	}
	else{
		$condition = "`$condition_field`='$condition_value' AND";
	}

	if($limit){
		$limit_query="LIMIT $limit";
		$i=0;
	}

	$result=mysqli_query($conn, "SELECT * FROM $table WHERE $condition cancel_status='0' ORDER BY id DESC $limit_query");
	while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
	{
		if($limit==1){
			return $row;
		}
		else{
			$output[$i] = $row;
			$i++;
		}
	}
	return $output;

	include 'conf/closedb.php';
}

function get_data_custom_query ($table, $condition, $limit) {
	include 'conf/config.php';
	include 'conf/opendb.php';

	if($limit){
		$limit_query="LIMIT $limit";
		$i=0;
	}

	$result=mysqli_query($conn, "SELECT * FROM $table $condition ORDER BY id DESC $limit_query");
	while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
	{
		if($limit==1){
			return $row;
		}
		else{
			$output[$i] = $row;
			$i++;
		}
	}
	return $output;

	include 'conf/closedb.php';
}

function get_data_count ($table, $condition) {
	include 'conf/config.php';
	include 'conf/opendb.php';

	$result=mysqli_query($conn, "SELECT COUNT(id) as count FROM $table $condition");
	while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
	{
		return $row['count'];
	}

	include 'conf/closedb.php';
}

function get_paginated_data ($table, $condition) {
	include 'conf/config.php';
	include 'conf/opendb.php';

	if($limit){
		$limit_query="LIMIT $limit";
		$i=0;
	}

	$result=mysqli_query($conn, "SELECT * FROM $table $condition");
	while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
	{
		$output[$i] = $row;
		$i++;
	}
	return $output;

	include 'conf/closedb.php';
}

function update_data($table, $data, $condition_field, $condition_value) {
	include 'conf/config.php';
	include 'conf/opendb.php';
	
	$columns = array_keys ( $data );
	$values = array_values ( $data );
	$loop = '';
	for($i = 0; $i < count ( $data ); $i ++) {
		if ($i == 0) {
			$loop = $loop . "`$columns[$i]`='$values[$i]'";
		} else {
			$loop = $loop . ", " . "`$columns[$i]`='$values[$i]'";
		}
	}

	mysqli_select_db ($conn, $dbname);
	$query = "UPDATE $table SET $loop WHERE `$condition_field`='$condition_value'";
	mysqli_query ($conn, $query );

	include 'conf/closedb.php';
}

function update_data_with_condition($table, $data, $condition) {
	include 'conf/config.php';
	include 'conf/opendb.php';
	
	$columns = array_keys ( $data );
	$values = array_values ( $data );
	$loop = '';
	for($i = 0; $i < count ( $data ); $i ++) {
		if ($i == 0) {
			$loop = $loop . "`$columns[$i]`='$values[$i]'";
		} else {
			$loop = $loop . ", " . "`$columns[$i]`='$values[$i]'";
		}
	}

	mysqli_select_db ($conn, $dbname);
	$query = "UPDATE $table SET $loop WHERE $condition";
	mysqli_query ($conn, $query );

	include 'conf/closedb.php';
}

function save_data($table, $data){
	include 'conf/config.php';
	include 'conf/opendb.php';

	$columns = array_keys ( $data );
	$values = array_values ( $data );
	$columns_query = '';
	$values_query = '';

	for($i = 0; $i < count ( $data ); $i ++) {
		if ($i == 0) {
			$columns_query = $columns_query . "`$columns[$i]`";
			$values_query = $values_query . "'$values[$i]'";
		} else {
			$columns_query = $columns_query . ", " . "`$columns[$i]`";
			$values_query = $values_query . ", " . "'$values[$i]'";
		}
	}

	mysqli_select_db($conn, $dbname);
	$query = "INSERT INTO $table ($columns_query) VALUES ($values_query)";
	mysqli_query($conn, $query) or die (mysqli_error($conn));

	include 'conf/closedb.php';
}

function check_data($table, $condition_field, $condition_value) {
	
	include 'conf/config.php';
	include 'conf/opendb.php';

	if(mysqli_num_rows(mysqli_query($conn, "SELECT id FROM $table WHERE $condition_field ='$condition_value'"))){
		return 1;
	}
	else{
		return 0;
	}


	include 'conf/closedb.php';
}

function generate_id($table, $prefix, $digit){
	include 'conf/config.php';
	include 'conf/opendb.php';

	$result=mysqli_query($conn, "SELECT MAX(id) AS id FROM $table");
	while($row = mysqli_fetch_array($result, MYSQLI_ASSOC))
	{
		$max_id=$row['id']+1;
		$max_id = str_pad($max_id, $digit, "0", STR_PAD_LEFT);
		$id=$prefix.$max_id;
		return $id;
	}
}

function pagination($page_no, $total, $total_page_no, $limit, $display_limit) {
	if($page_no+2 > $total_page_no){
		$max_page_no=$total_page_no;
	}
	else{
		$max_page_no=$page_no+2;
	}

	if($page_no-2 > 1){
		$min_page_no=$page_no-2;
	}
	else{
		$min_page_no=1;
	}

	$i=0;
	while($min_page_no<=$max_page_no){
		$page_nos[$i] = $min_page_no;
		$min_page_no++;
		$i++;
	}

	if(!$limit){
		$from=0;
	}
	else{
		$from=$limit;
	}

	if($total<$display_limit){
		$to=$total;
	}
	else{
		$to=$display_limit;
	}

	$range=$from.' - '. $to;

	$pagination_data = array('total' => $total,
	'range' => $range,
	'page_nos' => $page_nos,
	'current_page_no' => $page_no,
	'prev' => $page_no-1,
	'next' => $page_no+1,
	'min_page_no' => $min_page_no,
	'max_page_no' => $max_page_no
	 );

	return $pagination_data;
}