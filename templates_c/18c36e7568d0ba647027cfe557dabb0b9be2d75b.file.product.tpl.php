<?php /* Smarty version Smarty-3.0.8, created on 2017-09-18 20:32:50
         compiled from ".\templates\product/product.tpl" */ ?>
<?php /*%%SmartyHeaderCode:574659c01152aefb19-42494505%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '18c36e7568d0ba647027cfe557dabb0b9be2d75b' => 
    array (
      0 => '.\\templates\\product/product.tpl',
      1 => 1505759566,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '574659c01152aefb19-42494505',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if (!is_callable('smarty_modifier_escape')) include 'C:\xampp\htdocs\recycle.lk\libs\plugins\modifier.escape.php';
?><?php $_template = new Smarty_Internal_Template("common/header.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/top_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/nav_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<div id="content">
    <div class="container">

        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="index.php">Home</a>
                </li>
                <?php if ($_smarty_tpl->getVariable('type')->value){?>
                <li><a href="product.php?job=type&type=<?php echo $_smarty_tpl->getVariable('product_info')->value['sale'];?>
"><?php echo $_smarty_tpl->getVariable('product_info')->value['sale'];?>
</a>
                </li>
                <?php }?>
                <?php if ($_smarty_tpl->getVariable('product_info')->value['category']){?>
                <li><a href="product.php?job=category&category=<?php echo $_smarty_tpl->getVariable('product_info')->value['category'];?>
"><?php echo $_smarty_tpl->getVariable('product_info')->value['category'];?>
</a>
                </li>
                <?php }?>
                <?php if ($_smarty_tpl->getVariable('product_info')->value['sub_category']){?>
                <li><a href="product.php?job=category&sub_category=<?php echo $_smarty_tpl->getVariable('product_info')->value['sub_category'];?>
"><?php echo $_smarty_tpl->getVariable('product_info')->value['sub_category'];?>
</a>
                </li>
                <?php }?>
                <li><?php echo $_smarty_tpl->getVariable('product_info')->value['product_name'];?>
</li>
            </ul>

        </div>

        <div class="col-md-3">
            <!-- *** MENUS AND FILTERS ***-->
            <?php $_template = new Smarty_Internal_Template("common/category_sidebar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
        </div>

        <div class="col-md-9">

            <div class="row" id="productMain">
                <div class="col-sm-6">
                    <div id="mainImage">
                        <img src="<?php echo $_smarty_tpl->getVariable('product_info')->value['product_image'];?>
" alt="" class="img-responsive">
                    </div>

                    <div class="ribbon sale">
                        <div class="theribbon"><?php echo $_smarty_tpl->getVariable('product_info')->value['sale'];?>
</div>
                        <div class="ribbon-background"></div>
                    </div>
                    <!-- /.ribbon -->
                    <div class="row" id="thumbs">
                        <div class="col-xs-4">
                            <a href="<?php echo $_smarty_tpl->getVariable('product_info')->value['product_image'];?>
" class="thumb">
                                <img src="<?php echo $_smarty_tpl->getVariable('product_info')->value['product_image'];?>
" alt="" class="img-responsive">
                            </a>
                        </div>
                        <?php if ($_smarty_tpl->getVariable('product_info')->value['product_image2']){?>
                            <div class="col-xs-4">
                                <a href="<?php echo $_smarty_tpl->getVariable('product_info')->value['product_image2'];?>
" class="thumb">
                                    <img src="<?php echo $_smarty_tpl->getVariable('product_info')->value['product_image2'];?>
" alt="" class="img-responsive">
                                </a>
                            </div>
                        <?php }?>
                        <?php if ($_smarty_tpl->getVariable('product_info')->value['product_image3']){?>
                            <div class="col-xs-4">
                                <a href="<?php echo $_smarty_tpl->getVariable('product_info')->value['product_image3'];?>
" class="thumb">
                                    <img src="<?php echo $_smarty_tpl->getVariable('product_info')->value['product_image3'];?>
" alt="" class="img-responsive">
                                </a>
                            </div>                        
                        <?php }?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="box">
                        <h1 class="text-center"><?php echo $_smarty_tpl->getVariable('product_info')->value['product_name'];?>
</h1>
                        <p class="goToDescription"><a href="#details" class="scroll-to"><?php echo $_smarty_tpl->getVariable('product_info')->value['category'];?>
 | <?php echo $_smarty_tpl->getVariable('product_info')->value['sub_category'];?>
 | <?php echo $_smarty_tpl->getVariable('product_info')->value['label'];?>
</a>
                        </p>
                        <p class="price">Rs. <?php echo number_format($_smarty_tpl->getVariable('product_info')->value['price'],2,".",",");?>
</p>

                        <p class="text-center buttons">
                            <a class="btn btn-primary"data-toggle="collapse" data-target="#phone"><i class="fa fa-phone"></i> View Phone No</a> 
                            <a href="whishlist.php?job=add&product_id=<?php echo $_smarty_tpl->getVariable('product_info')->value['product_id'];?>
" class="btn btn-default"><i class="fa fa-heart"></i> Add to wishlist</a>
                        </p>

                        <h2 id="phone" class="collapse text-center">
                            <i class="fa fa-phone"></i> <?php echo $_smarty_tpl->getVariable('seller_info')->value['phone'];?>

                        </h2> 
                    </div>
                </div>

            </div>


            <div class="box" id="details">
                
                    <h4>Product details</h4>
                    <p><?php echo $_smarty_tpl->getVariable('product_info')->value['description'];?>
</p>

                    <hr>
                    <div class="">
                        <h4>Show it to your friends</h4>
                        
                            <a href="<?php echo $_smarty_tpl->getVariable('link')->value;?>
" onclick="return popitup('https://www.facebook.com/sharer/sharer.php?u=<?php echo $_smarty_tpl->getVariable('link')->value;?>
')" class="btn btn-social btn-facebook" data-animate-hover="pulse">
                                <i class="fa fa-facebook"></i> Share
                            </a>


                            <a href="<?php echo $_smarty_tpl->getVariable('link')->value;?>
" onclick="return popitup('https://twitter.com/intent/tweet?url=<?php echo $_smarty_tpl->getVariable('link')->value;?>
&text=<?php echo smarty_modifier_escape($_smarty_tpl->getVariable('page')->value,'quotes');?>
&via=Tanojan')" class="btn btn-social btn-twitter" data-animate-hover="pulse">
                                <i class="fa fa-twitter"></i> Tweet
                            </a>
                    </div>
            </div>

            <div class="row same-height-row">
                <?php $_template = new Smarty_Internal_Template("common/similar_products.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
            </div>

        </div>
        <!-- /.col-md-9 -->
    </div>
    <!-- /.container -->
</div>
<!-- /#content -->


<script>
function popitup(url) {
        newwindow=window.open(url,'name','height=400,width=600');
        if (window.focus) {newwindow.focus()}
        return false;
    }
</script>


<?php $_template = new Smarty_Internal_Template("common/footer.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/copyright.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/footer_js.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>