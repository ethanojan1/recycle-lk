<?php /* Smarty version Smarty-3.0.8, created on 2017-09-18 20:51:24
         compiled from ".\templates\common/category_sidebar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2268659c015ac3e1de1-56308265%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a1a0f9343785d8352c4e24ffaa98ccd31d732f3f' => 
    array (
      0 => '.\\templates\\common/category_sidebar.tpl',
      1 => 1505760670,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2268659c015ac3e1de1-56308265',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="panel panel-default sidebar-menu">

    <div class="panel-heading">
        <h3 class="panel-title">Categories</h3>
    </div>

    <div class="panel-body">
        <ul class="nav nav-pills nav-stacked category-menu"> 
            <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('categories')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
?>
                <li <?php if ($_smarty_tpl->getVariable('product_info')->value['category']==$_smarty_tpl->tpl_vars['category']->value['category']||$_smarty_tpl->getVariable('product_info')->value['category']==$_smarty_tpl->tpl_vars['category']->value['category']||$_smarty_tpl->getVariable('page')->value==$_smarty_tpl->tpl_vars['category']->value['category']){?> class="active" <?php }?>>
                    <a href="product.php?job=category&category=<?php echo $_smarty_tpl->tpl_vars['category']->value['category'];?>
&sub_category=no"><?php echo $_smarty_tpl->tpl_vars['category']->value['category'];?>
</a>
                    <ul>
                        <?php  $_smarty_tpl->tpl_vars['sub_category'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('sub_categories')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['sub_category']->key => $_smarty_tpl->tpl_vars['sub_category']->value){
?>
                            <?php if ($_smarty_tpl->tpl_vars['sub_category']->value['parent_category']==$_smarty_tpl->tpl_vars['category']->value['category']){?>
                                <li><a href="product.php?job=category&category=<?php echo $_smarty_tpl->tpl_vars['category']->value['category'];?>
&sub_category=<?php echo $_smarty_tpl->tpl_vars['sub_category']->value['category'];?>
"><i class="fa fa-caret-square-o-right" aria-hidden="true"></i> <?php echo $_smarty_tpl->tpl_vars['sub_category']->value['category'];?>
</a></li>
                            <?php }?>
                        <?php }} ?>
                    </ul>
                </li>

            <?php }} ?>
        </ul>

    </div>
</div>