<?php /* Smarty version Smarty-3.0.8, created on 2017-09-18 12:23:25
         compiled from ".\templates\product/list.tpl" */ ?>
<?php /*%%SmartyHeaderCode:643059bf9e9d606942-50988252%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9fbf3ed65c6238cf31c011f869f4fdd73eafbf54' => 
    array (
      0 => '.\\templates\\product/list.tpl',
      1 => 1505730202,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '643059bf9e9d606942-50988252',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("common/header.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/top_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/nav_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<div id="content">
    <div class="container">

        <div class="col-md-12">
            <ul class="breadcrumb">
                <li><a href="index.php">Home</a>
                </li>
                <?php if ($_smarty_tpl->getVariable('type')->value){?>
                <li><a href="product.php?job=type&type=<?php echo $_smarty_tpl->getVariable('type')->value;?>
"><?php echo $_smarty_tpl->getVariable('type')->value;?>
</a>
                </li>
                <?php }?>
                <?php if ($_smarty_tpl->getVariable('category_info')->value){?>
                <li><a href="product.php?job=category&category=<?php echo $_smarty_tpl->getVariable('category_info')->value;?>
"><?php echo $_smarty_tpl->getVariable('category_info')->value;?>
</a>
                </li>
                <?php }?>
                <?php if ($_smarty_tpl->getVariable('sub_category_info')->value){?>
                <li><a href="product.php?job=sub_category&sub_category=<?php echo $_smarty_tpl->getVariable('sub_category_info')->value;?>
"><?php echo $_smarty_tpl->getVariable('sub_category_info')->value;?>
</a>
                </li>
                <?php }?>
            </ul>

        </div>

        <div class="col-md-3">
            <!-- *** MENUS AND FILTERS ***-->
            <?php $_template = new Smarty_Internal_Template("common/category_sidebar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
        </div>

        <div class="col-md-9">
            <?php $_template = new Smarty_Internal_Template("common/pagination_product_count.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
            

            <div class="row products">

                <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('products')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value){
?>
                    <div class="col-md-4 col-sm-6">
                        <div class="product">
                            <div class="flip-container">
                                <div class="flipper">
                                    <div class="front">
                                        <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['product']->value['product_id'];?>
">
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['product']->value['product_image'];?>
" alt="" class="img-responsive" style="width: 100%;height: 200px;">
                                        </a>
                                    </div>
                                    <div class="back">
                                        <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['product']->value['product_id'];?>
">
                                            <img src="<?php echo $_smarty_tpl->tpl_vars['product']->value['product_image'];?>
" alt="" class="img-responsive" style="width: 100%;height: 200px;">
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['product']->value['product_id'];?>
" class="invisible">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['product']->value['product_image'];?>
" alt="" class="img-responsive" style="width: 100%; height: 200px;">
                            </a>
                            <div class="text">
                                <h3><a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['product']->value['product_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['product']->value['product_name'];?>
</a></h3>
                                <p class="price">Rs. <?php echo number_format($_smarty_tpl->tpl_vars['product']->value['price'],2,".",",");?>
</p>
                                <p class="buttons">
                                    <a href="detail.html" class="btn btn-success">View detail</a>
                                    
                                </p>
                            </div>
                            <!-- /.text -->
                            <?php if ($_smarty_tpl->tpl_vars['product']->value['sale']){?>
                            <div class="ribbon sale">
                                <div class="theribbon"><?php echo $_smarty_tpl->tpl_vars['product']->value['sale'];?>
</div>
                                <div class="ribbon-background"></div>
                            </div>
                            <?php }?>
                        </div>
                        <!-- /.product -->
                    </div>
                <?php }} ?>

            </div>

            <?php $_template = new Smarty_Internal_Template("common/pagination.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>


        </div>
        <!-- /.col-md-9 -->
    </div>
    <!-- /.container -->
</div>
<!-- /#content -->

<!-- Load Facebook SDK for JavaScript -->
  

<?php $_template = new Smarty_Internal_Template("common/footer.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/copyright.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/footer_js.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>