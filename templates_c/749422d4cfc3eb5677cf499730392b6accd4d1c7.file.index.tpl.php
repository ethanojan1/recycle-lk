<?php /* Smarty version Smarty-3.0.8, created on 2017-09-18 10:13:43
         compiled from ".\templates\index.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1067659bf8037c253a2-17805274%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '749422d4cfc3eb5677cf499730392b6accd4d1c7' => 
    array (
      0 => '.\\templates\\index.tpl',
      1 => 1505722403,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1067659bf8037c253a2-17805274',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("common/header.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/top_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/nav_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<div id="content">

    <div class="container">
        <div class="col-md-12">
            <div id="main-slider">
                <?php  $_smarty_tpl->tpl_vars['slider'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('sliders')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['slider']->key => $_smarty_tpl->tpl_vars['slider']->value){
?>
                    <div class="item slider_adjust">
                        <img src="<?php echo $_smarty_tpl->tpl_vars['slider']->value['image'];?>
" alt="" class="img-responsive">
                    </div>
                <?php }} ?>
            </div>
            <!-- /#main-slider -->
        </div>
    </div>

    <!-- *** ADVANTAGES HOMEPAGE *** -->
    <div id="advantages">

        
    </div>
    <!-- /#advantages -->

    <!-- *** ADVANTAGES END *** -->

    <!-- *** HOT PRODUCT SLIDESHOW ***
_________________________________________________________ -->
    <div id="hot">

        <div class="box padding0" >
            <div class="container">
                <div class="col-md-12">
                    <h1 class="text-center">Hot this week</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="product-slider">
                <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('products')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value){
?>

                <div class="item">
                    <div class="product">
                        <div class="flip-container">
                            <div class="flipper">
                                <div class="front">
                                    <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['product']->value['product_id'];?>
">
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['product']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                                <div class="back">
                                    <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['product']->value['product_id'];?>
">
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['product']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['product']->value['product_id'];?>
" class="invisible">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['product']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                        </a>
                        <div class="text">
                            <h3><a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['product']->value['product_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['product']->value['product_name'];?>
</a></h3>
                            <p class="price">Rs. <?php echo number_format($_smarty_tpl->tpl_vars['product']->value['price'],2,".",",");?>
</p>
                        </div>
                        <!-- /.text -->
                    </div>
                    <!-- /.product -->
                </div>
                <?php }} ?>

            </div>
            <!-- /.product-slider -->
        </div>
        <!-- /.container -->

    </div>

    <div id="hot">

        <div class="box padding0">
            <div class="container">
                <div class="col-md-12">
                    <h1 class="text-center">Products for Sale</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="product-slider">
                <?php  $_smarty_tpl->tpl_vars['sale'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('sales')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['sale']->key => $_smarty_tpl->tpl_vars['sale']->value){
?>

                <div class="item">
                    <div class="product">
                        <div class="flip-container">
                            <div class="flipper">
                                <div class="front">
                                    <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['sale']->value['product_id'];?>
">
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['sale']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                                <div class="back">
                                    <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['sale']->value['product_id'];?>
">
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['sale']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['sale']->value['product_id'];?>
" class="invisible">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['sale']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                        </a>
                        <div class="text">
                            <h3><a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['sale']->value['product_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['sale']->value['product_name'];?>
</a></h3>
                            <p class="price">Rs. <?php echo number_format($_smarty_tpl->tpl_vars['sale']->value['price'],2,".",",");?>
</p>
                        </div>
                        <!-- /.text -->
                    </div>
                    <!-- /.product -->
                </div>
                <?php }} ?>

            </div>
            <!-- /.product-slider -->
        </div>
        <!-- /.container -->

    </div>
    <!-- /#hot -->

    <div id="hot">

        <div class="box padding0">
            <div class="container">
                <div class="col-md-12">
                    <h1 class="text-center">Products as Gifts</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="product-slider">
                <?php  $_smarty_tpl->tpl_vars['gift'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('gifts')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['gift']->key => $_smarty_tpl->tpl_vars['gift']->value){
?>

                <div class="item">
                    <div class="product">
                        <div class="flip-container">
                            <div class="flipper">
                                <div class="front">
                                    <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['gift']->value['product_id'];?>
">
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['gift']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                                <div class="back">
                                    <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['gift']->value['product_id'];?>
">
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['gift']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['gift']->value['product_id'];?>
" class="invisible">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['gift']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                        </a>
                        <div class="text">
                            <h3><a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['gift']->value['product_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['gift']->value['product_name'];?>
</a></h3>
                            <p class="price">Rs. <?php echo number_format($_smarty_tpl->tpl_vars['gift']->value['price'],2,".",",");?>
</p>
                        </div>
                        <!-- /.text -->
                    </div>
                    <!-- /.product -->
                </div>
                <?php }} ?>

            </div>
            <!-- /.product-slider -->
        </div>
        <!-- /.container -->

    </div>
    <!-- /#hot -->

    <div id="hot">

        <div class="box padding0">
            <div class="container">
                <div class="col-md-12">
                    <h1 class="text-center">Products for Disposal</h1>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="product-slider">
                <?php  $_smarty_tpl->tpl_vars['disposal'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('disposals')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['disposal']->key => $_smarty_tpl->tpl_vars['disposal']->value){
?>

                <div class="item">
                    <div class="product">
                        <div class="flip-container">
                            <div class="flipper">
                                <div class="front">
                                    <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['disposal']->value['product_id'];?>
">
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['disposal']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                                <div class="back">
                                    <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['disposal']->value['product_id'];?>
">
                                        <img src="<?php echo $_smarty_tpl->tpl_vars['disposal']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                                    </a>
                                </div>
                            </div>
                        </div>
                        <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['disposal']->value['product_id'];?>
" class="invisible">
                            <img src="<?php echo $_smarty_tpl->tpl_vars['disposal']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                        </a>
                        <div class="text">
                            <h3><a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['disposal']->value['product_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['disposal']->value['product_name'];?>
</a></h3>
                            <p class="price">Rs. <?php echo number_format($_smarty_tpl->tpl_vars['disposal']->value['price'],2,".",",");?>
</p>
                        </div>
                        <!-- /.text -->
                    </div>
                    <!-- /.product -->
                </div>
                <?php }} ?>

            </div>
            <!-- /.product-slider -->
        </div>
        <!-- /.container -->

    </div>
    <!-- /#hot -->

    <!-- *** HOT END *** -->

   

</div>
<!-- /#content -->

<?php $_template = new Smarty_Internal_Template("common/footer.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/copyright.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/footer_js.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>