<?php /* Smarty version Smarty-3.0.8, created on 2017-09-18 20:51:36
         compiled from ".\templates\common/my_account_sidebar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:997459c015b8158f68-32386770%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a29a0f756a0fc6a02e168acdcf75db78227bf48b' => 
    array (
      0 => '.\\templates\\common/my_account_sidebar.tpl',
      1 => 1505732396,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '997459c015b8158f68-32386770',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="panel panel-default sidebar-menu">

    <div class="panel-heading">
        <h3 class="panel-title">Customer section</h3>
    </div>

    <div class="panel-body">

        <ul class="nav nav-pills nav-stacked">

            <li <?php if ($_smarty_tpl->getVariable('page')->value=="My Profile"){?>class="active"<?php }?>>
                <a href="my_account.php?job=profile"><i class="fa fa-user"></i> My account</a>
            </li>
            <li <?php if ($_smarty_tpl->getVariable('page')->value=="Sell Now"){?>class="active"<?php }?>>
                <a href="sell_now.php?job=sell"><i class="fa fa-money"></i> Sell Now</a>
            </li>
            <li <?php if ($_smarty_tpl->getVariable('page')->value=="My Products"){?>class="active"<?php }?>>
                <a href="sell_now.php?job=view"><i class="fa fa-money"></i> My Products</a>
            </li>
            <li>
                <a href="customer-wishlist.html"><i class="fa fa-heart"></i> My wishlist</a>
            </li>
            <li>
                <a href="index.html"><i class="fa fa-sign-out"></i> Logout</a>
            </li>
        </ul>
    </div>

    <?php if ($_smarty_tpl->getVariable('admin')->value==1){?>
        <div class="panel-heading">
            <h3 class="panel-title">Admin section</h3>
        </div>

        <div class="panel-body">

            <ul class="nav nav-pills nav-stacked">

                <li <?php if ($_smarty_tpl->getVariable('page')->value=="Slider"){?>class="active"<?php }?>>
                    <a href="slider.php?job=slider"><i class="fa fa-money"></i> Slider</a>
                </li>
                <li <?php if ($_smarty_tpl->getVariable('page')->value=="Highlight"){?>class="active"<?php }?>>
                    <a href="highlights.php?job=highlight"><i class="fa fa-user"></i> Highlight</a>
                </li>
                <li <?php if ($_smarty_tpl->getVariable('page')->value=="Category"){?>class="active"<?php }?>>
                    <a href="category.php?job=category"><i class="fa fa-list"></i> Category</a>
                </li>
                <li <?php if ($_smarty_tpl->getVariable('page')->value=="Label"){?>class="active"<?php }?>>
                    <a href="label.php?job=label"><i class="fa fa-list"></i> Label</a>
                </li>
            </ul>
        </div>
    <?php }?>

</div>