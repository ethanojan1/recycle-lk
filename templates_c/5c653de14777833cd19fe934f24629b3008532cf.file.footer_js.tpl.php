<?php /* Smarty version Smarty-3.0.8, created on 2017-09-10 20:12:21
         compiled from ".\templates\common/footer_js.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1266459b580855c92c0-28969562%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5c653de14777833cd19fe934f24629b3008532cf' => 
    array (
      0 => '.\\templates\\common/footer_js.tpl',
      1 => 1505066815,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1266459b580855c92c0-28969562',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
    </div>    <!-- /#all -->

    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    
    <script src="js/jquery.cookie.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/modernizr.js"></script>
    <script src="js/bootstrap-hover-dropdown.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/front.js"></script>    
	<script src="js/select2.full.min.js"></script>
    

</body>

</html>