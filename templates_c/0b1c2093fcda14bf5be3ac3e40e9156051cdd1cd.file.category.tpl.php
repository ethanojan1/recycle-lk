<?php /* Smarty version Smarty-3.0.8, created on 2017-09-01 11:15:33
         compiled from ".\templates\my_account/admin/category.tpl" */ ?>
<?php /*%%SmartyHeaderCode:559a92535810821-56323184%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0b1c2093fcda14bf5be3ac3e40e9156051cdd1cd' => 
    array (
      0 => '.\\templates\\my_account/admin/category.tpl',
      1 => 1504256970,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '559a92535810821-56323184',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("common/header.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/top_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/nav_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="#">Home</a>
                </li>
                <li>My account</li>
            </ul>

        </div>

        <div class="col-md-12">

            <?php $_template = new Smarty_Internal_Template("common/report.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
        
        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU *** -->
            <?php $_template = new Smarty_Internal_Template("common/my_account_sidebar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-4">
            <div class="box">
                <h1>Category</h1>
                <p class="lead">Add New Category</p>

                <form action="category.php?job=save&id=<?php echo $_smarty_tpl->getVariable('category_info')->value['id'];?>
" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="category">Category</label>
                                <input type="text" class="form-control" id="category" name="category" value="<?php echo $_smarty_tpl->getVariable('category_info')->value['category'];?>
" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="parent_category">Parent Category</label>
                                <select class="form-control" id="parent_category" name="parent_category">
                                    <?php if ($_smarty_tpl->getVariable('category_info')->value['parent_category']){?>
                                        <option value="<?php echo $_smarty_tpl->getVariable('category_info')->value['parent_category'];?>
"><?php echo $_smarty_tpl->getVariable('category_info')->value['parent_category'];?>
</option>
                                    <?php }else{ ?>
                                        <option disabled selected>Select Category</option>
                                    <?php }?>
                                    <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('categories')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
?>
                                        <?php if (!$_smarty_tpl->tpl_vars['category']->value['parent_category']){?>
                                            <option value="<?php echo $_smarty_tpl->tpl_vars['category']->value['category'];?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value['category'];?>
</option>
                                        <?php }?>
                                    <?php }} ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <?php if ($_smarty_tpl->getVariable('edit')->value=="On"){?>
                                <button type="submit" name="submit" value="Update" class="btn btn-primary"><i class="fa fa-money"></i> Update</button>
                            <?php }else{ ?>
                                <button type="submit" name="submit" value="Save" class="btn btn-primary"><i class="fa fa-money"></i> Save</button>
                            <?php }?>
                        </div>
                    </div>
                </form>  
            </div>
        </div>

        <div class="col-md-5">
            <div class="box">
                <h1>Categories</h1>
                <p class="lead">Current Categories</p>
                <div class="row">

                    <div class="table-responsive">          
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Edit</th>
                                    <th>Parent Category</th>
                                    <th>Category</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('categories')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
?>
                                    <tr>
                                        <td>
                                            <a href="category.php?job=edit&id=<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
">
                                                <div class="btn btn-sm btn-success"><i class="fa fa-pencil"></i></div>
                                            </a>
                                        </td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['category']->value['parent_category'];?>
</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['category']->value['category'];?>
</td>
                                        <td>
                                            <a href="category.php?job=delete&id=<?php echo $_smarty_tpl->tpl_vars['category']->value['id'];?>
">
                                                <div class="btn btn-sm btn-danger"><i class="fa fa-times"></i></div>
                                            </a>
                                        </td>
                                    </tr>
                                <?php }} ?>
                            </tbody>
                        </table>
                    </div>

                </div> 
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->


<?php $_template = new Smarty_Internal_Template("common/footer.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/copyright.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/footer_js.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>