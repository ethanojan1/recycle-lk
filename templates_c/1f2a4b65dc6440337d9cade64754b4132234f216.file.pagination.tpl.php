<?php /* Smarty version Smarty-3.0.8, created on 2017-09-18 08:23:06
         compiled from ".\templates\common/pagination.tpl" */ ?>
<?php /*%%SmartyHeaderCode:363659bf664a5350e0-46259485%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '1f2a4b65dc6440337d9cade64754b4132234f216' => 
    array (
      0 => '.\\templates\\common/pagination.tpl',
      1 => 1505715091,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '363659bf664a5350e0-46259485',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="pages">
    <ul class="pagination">
        <?php if ($_smarty_tpl->getVariable('current_page_no')->value>1){?>
        <li>
        <a href="product.php?job=pagination&page_no=<?php echo $_smarty_tpl->getVariable('prev')->value;?>
">&laquo;</a>
        </li>
        <?php }?>
        
        <?php  $_smarty_tpl->tpl_vars['page_no'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('page_nos')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['page_no']->key => $_smarty_tpl->tpl_vars['page_no']->value){
?>
        <li <?php if ($_smarty_tpl->getVariable('current_page_no')->value==$_smarty_tpl->tpl_vars['page_no']->value){?> class="active" <?php }?> >
            <a href="product.php?job=pagination&page_no=<?php echo $_smarty_tpl->tpl_vars['page_no']->value;?>
">
                <?php echo $_smarty_tpl->tpl_vars['page_no']->value;?>

            </a>
        </li>
        <?php }} ?>

        <?php if ($_smarty_tpl->getVariable('current_page_no')->value!=$_smarty_tpl->getVariable('max_page_no')->value){?>
        <li>
        <a href="product.php?job=pagination&page_no=<?php echo $_smarty_tpl->getVariable('next')->value;?>
">&raquo;</a>
        </li>
        <?php }?>
    </ul>
</div>