<?php /* Smarty version Smarty-3.0.8, created on 2017-08-31 11:27:12
         compiled from ".\templates\common/similar_products.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2250059a7d670f1c077-40727997%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2f71f9e3002abffe417a23a6fc4ee2d765268b5b' => 
    array (
      0 => '.\\templates\\common/similar_products.tpl',
      1 => 1504171453,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2250059a7d670f1c077-40727997',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="col-md-3 col-sm-6">
    <div class="box same-height green">
        <h3>You may also like these products</h3>
    </div>
</div>

<?php  $_smarty_tpl->tpl_vars['similar_product'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('similar_products')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['similar_product']->key => $_smarty_tpl->tpl_vars['similar_product']->value){
?>

    <?php if ($_smarty_tpl->tpl_vars['similar_product']->value['product_id']!=$_smarty_tpl->getVariable('product_info')->value['product_id']){?>

        <div class="col-md-3 col-sm-6">
            <div class="product same-height">
                <div class="flip-container">
                    <div class="flipper">
                        <div class="front">
                            <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['similar_product']->value['product_id'];?>
">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['similar_product']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                            </a>
                        </div>
                        <div class="back">
                            <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['similar_product']->value['product_id'];?>
">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['similar_product']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                            </a>
                        </div>
                    </div>
                </div>
                <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['similar_product']->value['product_id'];?>
" class="invisible">
                    <img src="<?php echo $_smarty_tpl->tpl_vars['similar_product']->value['product_image'];?>
" alt="" class="img-responsive product-image-home">
                </a>
                <div class="text">
                    <h3><a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['similar_product']->value['product_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['similar_product']->value['product_name'];?>
</a></h3>
                    <p class="price">Rs. <?php echo number_format($_smarty_tpl->tpl_vars['similar_product']->value['price'],2,".",",");?>
</p>
                </div>
            </div>
            <!-- /.product -->
        </div>
    <?php }?>

<?php }} ?>