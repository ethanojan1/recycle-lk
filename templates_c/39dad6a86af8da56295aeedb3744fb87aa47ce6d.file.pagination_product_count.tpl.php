<?php /* Smarty version Smarty-3.0.8, created on 2017-09-12 13:31:56
         compiled from ".\templates\common/pagination_product_count.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1563559b7c5ac8cde42-77125603%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '39dad6a86af8da56295aeedb3744fb87aa47ce6d' => 
    array (
      0 => '.\\templates\\common/pagination_product_count.tpl',
      1 => 1505215913,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1563559b7c5ac8cde42-77125603',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="box info-bar">
    <div class="row">
        <div class="col-sm-12 col-md-12 products-showing">
            <p> Displaying <strong><?php echo $_smarty_tpl->getVariable('range')->value;?>
</strong> products out of <strong><?php echo $_smarty_tpl->getVariable('total')->value;?>
</strong> products</p>
        </div>
    </div>
</div>