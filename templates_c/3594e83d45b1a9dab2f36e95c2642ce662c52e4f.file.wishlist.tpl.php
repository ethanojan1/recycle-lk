<?php /* Smarty version Smarty-3.0.8, created on 2017-09-11 06:08:12
         compiled from ".\templates\my_account/wishlist.tpl" */ ?>
<?php /*%%SmartyHeaderCode:833359b60c2ce3ad48-49467398%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '3594e83d45b1a9dab2f36e95c2642ce662c52e4f' => 
    array (
      0 => '.\\templates\\my_account/wishlist.tpl',
      1 => 1505102427,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '833359b60c2ce3ad48-49467398',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("common/header.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/top_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/nav_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="#">Home</a>
                </li>
                <li>My account</li>
            </ul>

        </div>

        <div class="col-md-12">

            <?php $_template = new Smarty_Internal_Template("common/report.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
        
        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU *** -->
            <?php $_template = new Smarty_Internal_Template("common/my_account_sidebar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-9">
            <div class="box">
                <h1>Wishlist</h1>
                <div class="row">
                    <?php  $_smarty_tpl->tpl_vars['product'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('wish_products')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['product']->key => $_smarty_tpl->tpl_vars['product']->value){
?>
                        <div class="item col-md-4 text-center">
                            <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['product']->value['product_id'];?>
" class="thumb">
                                <img src="<?php echo $_smarty_tpl->tpl_vars['product']->value['product_image'];?>
" alt="" class="img-responsive">
                            </a>
                            <h4><?php echo $_smarty_tpl->tpl_vars['product']->value['product_name'];?>
</h4>
                            <a href="wishlist.php?job=delete&product_id=<?php echo $_smarty_tpl->tpl_vars['product']->value['product_id'];?>
">
                                <div class="btn btn-sm btn-danger"><i class="fa fa-times"> Remove from Wishlist</i></div>
                            </a>
                            <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->tpl_vars['product']->value['product_id'];?>
">
                                <div class="btn btn-sm btn-success"><i class="fa fa-eye"> View Product</i></div>
                            </a>
                            <?php if ($_smarty_tpl->tpl_vars['product']->value['product_status']=="1"){?>
                                <div class="btn btn-sm btn-warning"><i class="fa fa-money"> Sold</i></div>
                            <?php }?>
                        </div>
                    <?php }} ?>
                </div> 
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->


<?php $_template = new Smarty_Internal_Template("common/footer.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/copyright.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/footer_js.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>