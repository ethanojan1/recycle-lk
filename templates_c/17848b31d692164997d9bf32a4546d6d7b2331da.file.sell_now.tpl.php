<?php /* Smarty version Smarty-3.0.8, created on 2017-09-10 20:22:44
         compiled from ".\templates\my_account/sell_now.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1591159b582f42f1dc5-36773363%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '17848b31d692164997d9bf32a4546d6d7b2331da' => 
    array (
      0 => '.\\templates\\my_account/sell_now.tpl',
      1 => 1505067627,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1591159b582f42f1dc5-36773363',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("common/header.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/top_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/nav_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="#">Home</a>
                </li>
                <li>My account</li>
            </ul>

        </div>

        <div class="col-md-12">

            <?php $_template = new Smarty_Internal_Template("common/report.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
        
        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU *** -->
            <?php $_template = new Smarty_Internal_Template("common/my_account_sidebar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-9">
            <div class="box">
                <h1>Sell Now!</h1>
                <p class="lead">Make Money out of unused stuffs. Sell them Here!</p>

                <form action="sell_now.php?job=save" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="product_name">Product Name</label>
                                <input type="text" class="form-control" id="product_name" name="product_name" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea class="form-control" id="description" name="description" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="sale">Sale Type</label>
                                <select class="form-control" id="sale" name="sale" onchange="priceAndSaleType(this);" required>
                                    <option disabled selected>Select Type</option>
                                    <option value="Sale">Sale</option>
                                    <option value="Gift">Gift</option>
                                    <option value="Disposal">Disposal</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input type="number" class="form-control" id="price" name="price" required>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="category">Product Category</label>
                                <select class="form-control" id="category" name="category" required>
                                    <option disabled selected>Select Category</option>
                                    <?php  $_smarty_tpl->tpl_vars['category'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('categories')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['category']->key => $_smarty_tpl->tpl_vars['category']->value){
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['category']->value['category'];?>
"><?php echo $_smarty_tpl->tpl_vars['category']->value['category'];?>
</option>
                                    <?php }} ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="sub_category">Product Category</label>
                                <select class="form-control" id="sub_category" name="sub_category" required>
                                    <option disabled selected>Select Category First</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 alert alert-warning collapse" id="price_tip">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label>Select Labels</label>
                                <select class="form-control multiselect" multiple="multiple" name="label[]">
                                    <?php  $_smarty_tpl->tpl_vars['label'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('labels')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['label']->key => $_smarty_tpl->tpl_vars['label']->value){
?>
                                        <option value="<?php echo $_smarty_tpl->tpl_vars['label']->value['label'];?>
"><?php echo $_smarty_tpl->tpl_vars['label']->value['label'];?>
</option>
                                    <?php }} ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="file" class="file" id="gallery" multiple name="product_image[]" required>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-money"></i> Sell Now</button>
                        </div>
                    </div>
                </form>

                
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->

<script>
function priceAndSaleType(input) {
    var saleType = input.value;

    if (saleType == "Gift") {
        document.getElementById("price_tip").innerHTML = "You cannot set price for gift";
        document.getElementById("price").innerHTML = "0";
        document.getElementById("price").readOnly = true; 
        $(".collapse").collapse('show'); 
    } 
    else if (saleType == "Disposal") {
        document.getElementById("price_tip").innerHTML = "You have to pay the money to the collecting person for disposing items";
        $(".collapse").collapse('show'); 
    } 
    else {
        document.getElementById("price_tip").innerHTML = "No price restriction for sales";
        $(".collapse").collapse('show');
    }
}
</script>

<script>
    $(document).on('ready', function() {


        $("#gallery").fileinput({
            browseClass: "btn btn-primary btn-block",
            showCaption: false,
            showRemove: false,
            showUpload: false
        });
    });
</script>

<script type="text/javascript"> 
    $(function() {
    
        $("#category").change(function() {
          $("#sub_category").load("ajax/sub_category_by_category.php?category=" + encodeURIComponent($("#category").val()));
        });
    });                 
</script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $(".multiselect").select2();
    
  });
</script>


<?php $_template = new Smarty_Internal_Template("common/footer.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/copyright.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/footer_js.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>