<?php /* Smarty version Smarty-3.0.8, created on 2017-08-29 11:04:19
         compiled from ".\templates\common/report.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1992359a52e13a8f144-25263293%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2c7d1499045983e0bebecc216f593a08cbbb4045' => 
    array (
      0 => '.\\templates\\common/report.tpl',
      1 => 1503997292,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1992359a52e13a8f144-25263293',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_smarty_tpl->getVariable('report')->value=="error"){?>
    <div class="alert alert-danger">
    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <h4>Error!</h4>
        <hr>
        <?php echo $_smarty_tpl->getVariable('message')->value;?>

    </div>
    
<?php }elseif($_smarty_tpl->getVariable('report')->value=="success"){?>
    <div class="alert alert-success">
    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <h4>Success!</h4>
        <hr>
        <?php echo $_smarty_tpl->getVariable('message')->value;?>

    </div>
<?php }?>