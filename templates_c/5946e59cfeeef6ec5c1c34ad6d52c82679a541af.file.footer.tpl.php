<?php /* Smarty version Smarty-3.0.8, created on 2017-09-18 10:34:30
         compiled from ".\templates\common/footer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2523159bf8516721e70-31358404%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '5946e59cfeeef6ec5c1c34ad6d52c82679a541af' => 
    array (
      0 => '.\\templates\\common/footer.tpl',
      1 => 1505723660,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2523159bf8516721e70-31358404',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div id="footer" data-animate="fadeInUp">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <h4>Pages</h4>

                <ul>
                    <li><a href="about.php">About us</a>
                    </li>
                    <li><a href="term.php">Terms and conditions</a>
                    </li>
                    <li><a href="faq.php">FAQ</a>
                    </li>
                    <li><a href="contact.php">Contact us</a>
                    </li>
                </ul>

                <hr class="hidden-md hidden-lg hidden-sm">

            </div>
            <!-- /.col-md-3 -->

            <div class="col-md-3 col-sm-6">

                <h4>Products Type</h4>

                <ul>
                    <li><a href="product.php?job=type&type=Sale">Sale</a>
                    </li>
                    <li><a href="product.php?job=type&type=Gift">Gift</a>
                    </li>
                    <li><a href="product.php?job=type&type=Sale">Disposal</a>
                    </li>
                </ul>

                <hr class="hidden-md hidden-lg">

            </div>
            <!-- /.col-md-3 -->

            <div class="col-md-3 col-sm-6">

                <h4>Get the news</h4>

                <p class="text-muted">Subscribe now and get all the important news about us.</p>

                <form action="register.php?job=subscribe" method="POST">
                    <div class="input-group">

                        <input type="email" name="email" class="form-control" required>

                        <span class="input-group-btn">
                            <button class="btn btn-success" type="submit">Subscribe!</button>
                        </span>

                    </div>
                    <!-- /input-group -->
                </form>

                <hr class="hidden-md hidden-lg">

            </div>
            <!-- /.col-md-3 -->

            <div class="col-md-3 col-sm-6">

                <h4>Stay in touch</h4>

                <p class="social">
                    <a href="#" class="facebook external" data-animate-hover="shake"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="twitter external" data-animate-hover="shake"><i class="fa fa-twitter"></i></a>
                </p>


            </div>
            <!-- /.col-md-3 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.container -->
</div>