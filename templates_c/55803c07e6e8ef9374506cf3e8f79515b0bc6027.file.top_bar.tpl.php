<?php /* Smarty version Smarty-3.0.8, created on 2017-09-18 12:15:10
         compiled from ".\templates\common/top_bar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2525159bf9cae58a180-78044197%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '55803c07e6e8ef9374506cf3e8f79515b0bc6027' => 
    array (
      0 => '.\\templates\\common/top_bar.tpl',
      1 => 1505729240,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2525159bf9cae58a180-78044197',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div id="top">
    <div class="container">
        <div class="col-md-6 offer" data-animate="fadeInDown">
            <?php if ($_smarty_tpl->getVariable('last_disposal')->value){?>
                <a href="product.php?job=type&type=Disposal" class="btn btn-danger btn-sm" data-animate-hover="shake">
                    Hot: Disposing product
                </a>
                <a href="product.php?job=view&product_id=<?php echo $_smarty_tpl->getVariable('last_disposal')->value['product_id'];?>
">
                   <?php echo $_smarty_tpl->getVariable('last_disposal')->value['category'];?>
 | <?php echo $_smarty_tpl->getVariable('last_disposal')->value['product_name'];?>
 | Rs. <?php echo number_format($_smarty_tpl->getVariable('last_disposal')->value['price'],2,".",",");?>

                </a>
            <?php }?>
        </div>
        <div class="col-md-6" data-animate="fadeInDown">
            <ul class="menu">
                <?php if ($_smarty_tpl->getVariable('login')->value==1){?>
                    <li><a href="my_account.php?job=profile" class="btn btn-danger btn-sm"><?php echo $_smarty_tpl->getVariable('first_name')->value;?>
</a> 
                    </li>
                    <li><a href="my_account.php?job=profile">My Account</a> 
                    </li>
                    <li><a href="login.php?job=logout">Logout</a>
                    </li>
                <?php }else{ ?>
                    <li><a href="#" data-toggle="modal" data-target="#login-modal">Login</a>
                    </li>
                    <li><a href="register.php?job=register">Register</a>
                    </li>
                <?php }?>     
            </ul>
        </div>
    </div>
    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
        <div class="modal-dialog modal-sm">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="Login">Customer login</h4>
                </div>
                <div class="modal-body">
                    <form action="login.php?job=login" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" id="email-modal" name="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="password-modal" name="password" placeholder="Password">
                        </div>

                        <p class="text-center">
                            <button class="btn btn-primary"><i class="fa fa-sign-in"></i> Log in</button>
                        </p>

                    </form>

                    <p class="text-center text-muted">Not registered yet?</p>
                    <p class="text-center text-muted"><a href="register.php?job=register"><strong>Register now</strong></a>! It is easy and done in 1&nbsp;minute and gives you access to much more!</p>

                </div>
            </div>
        </div>
    </div>

</div>