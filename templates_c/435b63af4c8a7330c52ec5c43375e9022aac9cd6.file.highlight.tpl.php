<?php /* Smarty version Smarty-3.0.8, created on 2017-08-31 11:55:22
         compiled from ".\templates\my_account/admin/highlight.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1059359a7dd0a9983e7-81861756%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '435b63af4c8a7330c52ec5c43375e9022aac9cd6' => 
    array (
      0 => '.\\templates\\my_account/admin/highlight.tpl',
      1 => 1504173315,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1059359a7dd0a9983e7-81861756',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<?php $_template = new Smarty_Internal_Template("common/header.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/top_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/nav_bar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>

<div id="content">
    <div class="container">

        <div class="col-md-12">

            <ul class="breadcrumb">
                <li><a href="#">Home</a>
                </li>
                <li>My account</li>
            </ul>

        </div>

        <div class="col-md-12">

            <?php $_template = new Smarty_Internal_Template("common/report.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
        
        </div>

        <div class="col-md-3">
            <!-- *** CUSTOMER MENU *** -->
            <?php $_template = new Smarty_Internal_Template("common/my_account_sidebar.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
            <!-- *** CUSTOMER MENU END *** -->
        </div>

        <div class="col-md-4">
            <div class="box">
                <h1>highlight</h1>
                <p class="lead">Add New highlight Image</p>

                <form action="highlights.php?job=save&id=<?php echo $_smarty_tpl->getVariable('highlight_info')->value['id'];?>
" method="POST" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" class="form-control" id="title" name="title" value="<?php echo $_smarty_tpl->getVariable('highlight_info')->value['title'];?>
" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="text">Text</label>
                                <input type="text" class="form-control" id="text" name="text" value="<?php echo $_smarty_tpl->getVariable('highlight_info')->value['text'];?>
" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="font">Font</label>
                                <input type="text" class="form-control" id="font" name="font" value="<?php echo $_smarty_tpl->getVariable('highlight_info')->value['font'];?>
" required>
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                    <div class="row">
                        <div class="col-sm-12">
                            <?php if ($_smarty_tpl->getVariable('edit')->value=="On"){?>
                                <button type="submit" name="submit" value="Update" class="btn btn-primary"><i class="fa fa-money"></i> Update</button>
                            <?php }else{ ?>
                                <button type="submit" name="submit" value="Save" class="btn btn-primary"><i class="fa fa-money"></i> Save</button>
                            <?php }?>
                        </div>
                    </div>
                </form>  
            </div>
        </div>

        <div class="col-md-5">
            <div class="box">
                <h1>highlights</h1>
                <p class="lead">Current highlight Images</p>
                <div class="row">
                    <?php  $_smarty_tpl->tpl_vars['highlight'] = new Smarty_Variable;
 $_from = $_smarty_tpl->getVariable('highlights')->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
if ($_smarty_tpl->_count($_from) > 0){
    foreach ($_from as $_smarty_tpl->tpl_vars['highlight']->key => $_smarty_tpl->tpl_vars['highlight']->value){
?>
                        <div class="item col-md-6 text-center">
                            <i class="<?php echo $_smarty_tpl->tpl_vars['highlight']->value['font'];?>
"></i>
                            <h4><?php echo $_smarty_tpl->tpl_vars['highlight']->value['title'];?>
</h4>
                            <p><?php echo $_smarty_tpl->tpl_vars['highlight']->value['text'];?>
</p>
                            <a href="highlights.php?job=delete&id=<?php echo $_smarty_tpl->tpl_vars['highlight']->value['id'];?>
">
                                <div class="btn btn-sm btn-danger"><i class="fa fa-times"> Delete</i></div>
                            </a>
                            <a href="highlights.php?job=edit&id=<?php echo $_smarty_tpl->tpl_vars['highlight']->value['id'];?>
">
                                <div class="btn btn-sm btn-success"><i class="fa fa-times"> Edit</i></div>
                            </a>
                        </div>
                    <?php }} ?>
                </div> 
            </div>
        </div>

    </div>
    <!-- /.container -->
</div>
<!-- /#content -->


<?php $_template = new Smarty_Internal_Template("common/footer.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/copyright.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>
<?php $_template = new Smarty_Internal_Template("common/footer_js.tpl", $_smarty_tpl->smarty, $_smarty_tpl, $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null);
 echo $_template->getRenderedTemplate(); $_template->rendered_content = null;?><?php unset($_template);?>