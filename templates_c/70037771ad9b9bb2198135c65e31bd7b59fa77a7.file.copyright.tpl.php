<?php /* Smarty version Smarty-3.0.8, created on 2017-09-18 10:26:30
         compiled from ".\templates\common/copyright.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1423259bf8336bcfe81-51143308%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '70037771ad9b9bb2198135c65e31bd7b59fa77a7' => 
    array (
      0 => '.\\templates\\common/copyright.tpl',
      1 => 1505723183,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1423259bf8336bcfe81-51143308',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div id="copyright">
    <div class="container">
        <div class="col-md-6">
            <p class="pull-left">© <?php echo date('Y');?>
, Recycle.lk</p>

        </div>
        <div class="col-md-6">
            <p class="pull-right">Developed by <a href="https://thekoders.com">The Koder</a>
                 <!-- Not removing these links is part of the license conditions of the template. Thanks for understanding :) If you want to use the template without the attribution links, you can do so after supporting further themes development at https://bootstrapious.com/donate  -->
            </p>
        </div>
    </div>
</div>