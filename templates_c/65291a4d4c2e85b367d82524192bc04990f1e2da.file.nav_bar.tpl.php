<?php /* Smarty version Smarty-3.0.8, created on 2017-09-18 12:17:55
         compiled from ".\templates\common/nav_bar.tpl" */ ?>
<?php /*%%SmartyHeaderCode:648959bf9d53d068a9-48988282%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '65291a4d4c2e85b367d82524192bc04990f1e2da' => 
    array (
      0 => '.\\templates\\common/nav_bar.tpl',
      1 => 1505729872,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '648959bf9d53d068a9-48988282',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<div class="navbar navbar-default yamm" role="navigation" id="navbar">
    <div class="container">
        <div class="navbar-header">

            <a class="navbar-brand home" href="index.php" data-animate-hover="bounce">
                <img src="img/logo.png" alt="Recycle.LK" class="hidden-xs">
                <img src="img/logo.png" alt="Recycle.LK" class="visible-xs"><span class="sr-only">Recycle.LK</span>
            </a>
            <div class="navbar-buttons">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="fa fa-align-justify"></i>
                </button>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#search">
                    <span class="sr-only">Toggle search</span>
                    <i class="fa fa-search"></i>
                </button>
                <a class="btn btn-default navbar-toggle" href="basket.html">
                    <i class="fa fa-shopping-cart"></i>  <span class="hidden-xs">3 items in cart</span>
                </a>
            </div>
        </div>
        <!--/.navbar-header -->

        <div class="navbar-collapse collapse" id="navigation">

            <ul class="nav navbar-nav navbar-left">
                <li <?php if ($_smarty_tpl->getVariable('page')->value=="Home"){?> class="active" <?php }?>>
                    <a href="index.php">Home</a>
                </li>
                <li <?php if ($_smarty_tpl->getVariable('type')->value=="Sale"){?> class="active" <?php }?>>
                    <a href="product.php?job=type&type=Sale">Sales</a>
                </li>

                <li <?php if ($_smarty_tpl->getVariable('type')->value=="Gift"){?> class="active" <?php }?>>
                    <a href="product.php?job=type&type=Gift">Gift</a>
                </li>

                <li <?php if ($_smarty_tpl->getVariable('type')->value=="Disposal"){?> class="active" <?php }?>>
                    <a href="product.php?job=type&type=Disposal">Disposal</a>
                </li>
            </ul>

        </div>
        <!--/.nav-collapse -->

        <div class="navbar-buttons">

            <div class="navbar-collapse collapse right" id="search-not-mobile">
                <form class="navbar-form" role="search" action="product.php?job=search" method="POST">
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search" required>
                        <span class="input-group-btn">

                            <button type="submit" class="btn btn-primary btn-special"><i class="fa fa-search"></i></button>

                        </span>
                    </div>
                </form>
            </div>

        </div>
        <!--/.nav-collapse -->

    </div>
    <!-- /.container -->
</div>
<!-- /#navbar -->
