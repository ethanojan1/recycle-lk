<?php /* Smarty version Smarty-3.0.8, created on 2017-09-18 20:28:52
         compiled from ".\templates\common/header.tpl" */ ?>
<?php /*%%SmartyHeaderCode:2920559c01064e28f08-27785501%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'eccde56bb1daecda477418c6e6142b66b0f4ff4b' => 
    array (
      0 => '.\\templates\\common/header.tpl',
      1 => 1505731743,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '2920559c01064e28f08-27785501',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
)); /*/%%SmartyHeaderCode%%*/?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="An Echo Friendly Network">
    <meta name="author" content="The Koders">
    <meta name="keywords" content="Recycle, Disposal, Gift">

    <title>
        Recycle.LK || <?php echo $_smarty_tpl->getVariable('page')->value;?>

    </title>

    <meta name="keywords" content="">

    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/animate.min.css" rel="stylesheet">
    <link href="css/owl.carousel.css" rel="stylesheet">
    <link href="css/owl.theme.css" rel="stylesheet">
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/themes/base/minified/jquery-ui.min.css" type="text/css" /> 

    <!-- theme stylesheet -->
    <link href="css/style.blue.css" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="css/custom.css" rel="stylesheet">

    <script src="js/respond.min.js"></script>

    <link rel="shortcut icon" href="favicon.png">

    <link rel="stylesheet" href="css/fileinput.css">

    <link rel="stylesheet" href="css/select2.min.css">


    <script src="js/jquery-2.2.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/fileinput.js"></script>
    <script src="js/fileinput.min.js"></script>

<script type="text/javascript" src="js/jquery-ui.min.js"></script>


</head>

<body>
    <div id="all">