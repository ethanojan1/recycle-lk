<?php
require_once 'conf/smarty-conf.php';
include 'functions/common_functions.php';

if($_SESSION['login']==1){
	//defaults for the file
	$table="inventory";
	$condition_field="product_id";
	$condition_value=$_REQUEST['product_id'];

	if($_REQUEST['job']=="sell"){
		$condition="WHERE `parent_category`='' AND cancel_status='0' ORDER BY category ASC";
		$categories=get_data_custom_query ('category', $condition, '');
		$labels=get_data('label', '', '', '');

		$smarty->assign('labels', $labels);
		$smarty->assign('categories', $categories);
		$smarty->assign('page',"Sell Now");
		$smarty->display('my_account/sell_now.tpl');
	}

	elseif($_REQUEST['job']=="save"){
		$product_id= generate_id($table, 'ITEM-', 7);
		
		$nproduct_image = count($_FILES['product_image'] ['name']);
		for($i=0; $i < $nproduct_image; $i++)
		{
			$filename = stripslashes ($_FILES['product_image'] ['name'][$i]);
			$file_name=$product_id.'.'.$filename;
			$product_image="product_image/".$file_name;
			$copied = copy($_FILES['product_image']['tmp_name'][$i],$product_image);

			$image[$i]=$product_image;
		}

		$nlabel = count($_POST['label']);	 
		for($i=0; $i < $nlabel; $i++)
		{
			$label_text=$_POST['label'][$i].', '.$label_text;
		}

		$data = array('product_id' => $product_id,
			'product_name' => addslashes($_POST ['product_name']), 
			'description' => addslashes($_POST ['description']),
			'price' => $_POST ['price'],
			'sale' => $_POST ['sale'],
			'category' => $_POST ['category'],
			'sub_category' => $_POST['sub_category'],
			'label' => $label_text,
			'product_image' => $image[0],
			'product_image_1' => $image[1],
			'product_image_2' => $image[2],
			'user_id' => $_SESSION['user_id']
		);

		save_data($table, $data);
		$condition="WHERE `parent_category`='' AND cancel_status='0' ORDER BY category ASC";
		$categories=get_data_custom_query ('category', $condition, '');
		$labels=get_data('label', '', '', '');

		$smarty->assign('labels', $labels);
		$smarty->assign('categories', $categories);
		$smarty->assign('page',"Sell Now");
		$smarty->display('my_account/sell_now.tpl');		
	}

	elseif($_REQUEST['job']=="edit"){
		
	}

	elseif($_REQUEST['job']=="view"){
		$products=get_data($table, 'user_id', $_SESSION['user_id'], '');

		$smarty->assign('products', $products);
		$smarty->assign('page',"My Products");
		$smarty->display('my_account/my_product.tpl');
	}

	elseif($_REQUEST['job']=="delete"){
		$data = array('cancel_status' => 1);

		update_data($table, $data, 'id', $_REQUEST['id']);
		
		$products=get_data($table, 'user_id', $_SESSION['user_id'], '');

		$smarty->assign('products', $products);
		$smarty->assign('page',"My Products");
		$smarty->display('my_account/my_product.tpl');
	}

	elseif($_REQUEST['job']=="change_status"){
		$data = array('product_status' => 1,
			'cancel_status', 1
		);

		update_data($table, $data, 'id', $_REQUEST['id']);
		
		$products=get_data($table, 'user_id', $_SESSION['user_id'], '');

		$smarty->assign('products', $products);
		$smarty->assign('page',"My Products");
		$smarty->display('my_account/my_product.tpl');
	}

	else{
		$user_info=get_data($table, $condition_field, $condition_value, '1');
		$condition="WHERE `parent_category`='' AND cancel_status='0' ORDER BY category ASC";
		$categories=get_data_custom_query ('category', $condition, '');
		$labels=get_data('label', '', '', '');

		$smarty->assign('labels', $labels);
		$smarty->assign('categories', $categories);
		$smarty->assign('user_info', $user_info);
		$smarty->assign('page',"My Profile");
		$smarty->display('my_account/profile.tpl');
	}
}

else{
	$smarty->assign('page',"Login");
	$smarty->display('register/register.tpl');	
}