<?php
require_once 'conf/smarty-conf.php';
include 'functions/common_functions.php';

//defaults for the file
$table="wishlist";
$condition_field="user_id";
$condition_value=$_SESSION['user_id'];

if($_SESSION['login']==1){

	if($_REQUEST['job']=="add"){
		$data = array('product_id' => $_REQUEST['product_id'],
			'user_id' => $_SESSION['user_id']
		);

		save_data($table, $data);

		$product_info=get_data('inventory', 'product_id', $_REQUEST['product_id'], '1');
		$similar_products=get_data('inventory', 'category', $product_info['category'], '3');
		$seller_info=get_data('public_users', 'user_id', $product_info['user_id'], '1');

		$smarty->assign('product_info', $product_info);
		$smarty->assign('seller_info', $seller_info);
		$smarty->assign('similar_products', $similar_products);
		$smarty->assign('page', $product_info['product_name']);
		$smarty->display('product/product.tpl');
	}

	elseif($_REQUEST['job']=="list"){
		$wishlist_info=get_data($table, $condition_field, $condition_value, '');

		if (count($wishlist_info)>=1){
			$i=0;
			$products  = array();
			foreach ($wishlist_info as $product) {
				
				$product_info=get_data('inventory', 'product_id', $product['product_id'], '1');
				$wish_products[$i]= $product_info;
				$i++;
			}
		}

		$smarty->assign('wish_products', $wish_products);
		$smarty->assign('page', "Wishlist");
		$smarty->display('my_account/wishlist.tpl');
	}

	elseif($_REQUEST['job']=="delete"){
		$data = array('cancel_status' => 1);

		$condition = "user_id='$_SESSION[user_id]' AND product_id='$_REQUEST[product_id]'";
		update_data_with_condition($table, $data, $condition);
		
		$wishlist_info=get_data($table, $condition_field, $condition_value, '');

		if (count($wishlist_info)>=1){
			$i=0;
			$products  = array();
			foreach ($wishlist_info as $product) {
				
				$product_info=get_data('inventory', 'product_id', $product['product_id'], '1');
				$wish_products[$i]= $product_info;
				$i++;
			}
		}

		$smarty->assign('wish_products', $wish_products);
		$smarty->assign('page', "Wishlist");
		$smarty->display('my_account/wishlist.tpl');
	}

}

else{
	$smarty->assign('page',"Login");
	$smarty->display('register/register.tpl');	
}
