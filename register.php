<?php
require_once 'conf/smarty-conf.php';

//include 'functions/user_functions.php';
//include 'functions/cart_functions.php';
//include 'functions/list_functions.php';
include 'functions/common_functions.php';
//include 'functions/wishlists_functions.php';
include 'mail/class.phpmailer.php';
include 'mail/class.pop3.php';


//defaults for the file
$table="public_users";
$condition_field="user_id";
$condition_value=$_SESSION['user_id'];

if($_REQUEST['job']=="register"){

	$smarty->assign('page',"Register");
	$smarty->display('register/register.tpl');	
}

elseif ($_REQUEST['job']=="save"){

	$_SESSION ['user_id']= $user_id= generate_id($table, 'USER-', 5);

	$data = array('user_id' => $user_id,
		'email' => $_POST ['email'],
		'sub_email' => $_POST ['email'],
		'password' => md5($_POST ['password']),
		'first_name' => $_POST ['first_name'],
		'last_name' => $_POST ['last_name'],
		'hash_key' => md5($_POST ['email']),
		'role' => "User"
		);

	if (check_data($table, 'sub_email', $_POST ['email'])==1 AND check_data($table, 'email', $_POST ['email'])==0){

		update_data($table, $data, 'sub_email', $_POST ['email']);
		
		$smarty->assign('report',"success");
		$smarty->assign('message',"Please Login with your credentials.");
		$smarty->display('register/register.tpl');	
	
	}
	elseif (check_data($table, 'email', $_POST ['email'])==0){
	
		save_data($table, $data);
		
		$smarty->assign('report',"success");
		$smarty->assign('message',"Please Login with your credentials.");
		$smarty->display('register/register.tpl');	
	
	}
	else{
		
		$smarty->assign('report',"error");
		$smarty->assign('message',"This Email Address Already Exist.");
		$smarty->assign('page',"Login");
		$smarty->display('register/register.tpl');	
	}

}

elseif ($_REQUEST['job']=="subscribe"){

	if (check_data($table, 'email', $_POST ['email'])==0 OR check_data($table, 'sub_email', $_POST ['email'])==0){
		$user_id= generate_id($table, 'SUBSCRIBER-', 5);

		$data = array('user_id' => $user_id,
			'sub_email' => $_POST ['email'],
			'role' => "Subscriber"
		);

		save_data($table, $data);
		
		$smarty->assign('report',"success");
		$smarty->assign('message',"Subscription Successful.");
		$smarty->display('register/register.tpl');	
	
	}else{
		
		$smarty->assign('report',"error");
		$smarty->assign('message',"This Email Address Already in Subscription list.");
		$smarty->assign('page',"Login");
		$smarty->display('register/register.tpl');	
	}

}

else{
	if($_SESSION['login']==1){
		$smarty->assign('page',"Home");
		$smarty->display('index.tpl');
	}
	else{
		$smarty->assign('page',"Register");
		$smarty->display('register/register.tpl');
	}
}
?>