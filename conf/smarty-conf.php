<?php

session_start();
ini_set("session.gc_maxlifetime", "151200"); 

require('libs/Smarty.class.php');
require('libs/SmartyPaginate.class.php');
$smarty = new Smarty;

ini_set ('displayerrors', true);
error_reporting (E_ALL + E_NOTICE);

$first_name=$_SESSION['first_name'];
$smarty->assign('first_name', $first_name);

if($_SESSION['login']==1 || $_SESSION['temp_id']){
	$smarty->assign('login', 1);
	echo'<input type="hidden" id="login_check" value="1" />';
}
else{
	echo'<input type="hidden" id="login_check" value="0" />';
}


$link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$smarty->assign('link', $link);
if($_SESSION['role']=="Admin"){
	$smarty->assign('admin', 1);
}

$display_limit=9;
