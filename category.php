<?php
require_once 'conf/smarty-conf.php';
include 'functions/common_functions.php';

if($_SESSION['login']==1){
	//defaults for the file
	$table="category";
	$condition_field="id";
	$condition_value=$_REQUEST['id'];

	if($_SESSION['role']=="Admin"){

		if($_REQUEST['job']=="category"){
			$categories=get_data('category', '', '', '');

			$smarty->assign('categories', $categories);
			$smarty->assign('page',"Category");
			$smarty->display('my_account/admin/category.tpl');
		}

		elseif($_REQUEST['job']=="save"){
			$data = array('category' => $_POST['category'],
				'parent_category' => $_POST['parent_category']
				);

			if($_REQUEST['submit']=="Save"){
				save_data($table, $data);
			}
			else{
				update_data($table, $data, $condition_field, $condition_value);
			}
			
			$categories=get_data('category', '', '', '');

			$smarty->assign('categories', $categories);
			$smarty->assign('page',"Category");
			$smarty->display('my_account/admin/category.tpl');
		}

		elseif($_REQUEST['job']=="edit"){
			$category_info=get_data($table, $condition_field, $condition_value, '1');
			
			$categories=get_data('category', '', '', '');

			$smarty->assign('edit', "On");
			$smarty->assign('categories', $categories);
			$smarty->assign('category_info', $category_info);
			$smarty->assign('page',"Category");
			$smarty->display('my_account/admin/category.tpl');
		}


		elseif($_REQUEST['job']=="delete"){
			
			$data = array('cancel_status' => 1);

			update_data($table, $data, $condition_field, $condition_value);
			
			$categories=get_data('category', '', '', '');

			$smarty->assign('categories', $categories);
			$smarty->assign('page',"Category");
			$smarty->display('my_account/admin/category.tpl');
		}

		else{
			$categories=get_data('category', '', '', '');

			$smarty->assign('categories', $categories);
			$smarty->assign('page',"Category");
			$smarty->display('my_account/admin/category.tpl');
		}
	}
	else{
		$smarty->assign('report',"error");
		$smarty->assign('message',"You don't have admin permission.");
		$smarty->display('my_account/profile.tpl');
	}
}

else{
	$smarty->assign('page',"Login");
	$smarty->display('register/register.tpl');	
}
	